package ex;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import javax.inject.Named;
import javax.interceptor.Interceptors;

@Named
@Stateless
@Interceptors(AnotherClassInterceptor.class)
public class Another {
    @PostConstruct
    public void init() {
        say("PostConstruct");
    }

    @PreDestroy
    public void destroy() {
        say("PreDestroy");
    }

    public String getData() {
        return "A data";
    }

    void say(String s) {
        System.out.println("Another: " + s);
    }
}
