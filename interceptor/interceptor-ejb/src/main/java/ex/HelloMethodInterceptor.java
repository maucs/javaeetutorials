package ex;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class HelloMethodInterceptor {
    @AroundInvoke
    public Object log(InvocationContext ctx) throws Exception {
        say(ctx.getMethod().toString());
        Object[] parameters = ctx.getParameters();
        for (int i = 0; i < parameters.length; i++)
            say("Parameters: " + parameters[i]);
        return ctx.proceed();
    }

    public void say(String s) {
        System.out.println("HelloMethodInterceptor: " + s);
    }
}