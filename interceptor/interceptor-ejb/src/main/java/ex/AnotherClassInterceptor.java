package ex;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.interceptor.AroundConstruct;
import javax.interceptor.InvocationContext;

public class AnotherClassInterceptor {
    @AroundConstruct
    public Object init(InvocationContext ctx) throws Exception {
        say("Interceptor AroundConstructor invoked");
        return ctx.proceed();
    }

    @PostConstruct
    public Object postConstruct(InvocationContext ctx) throws Exception {
        say("Interceptor PostConstruct invoked");
        return ctx.proceed();
    }

    @PreDestroy
    public Object preDestroy(InvocationContext ctx) throws Exception {
        say("Interceptor PreDestroy invoked");
        return ctx.proceed();
    }

    public void say(String s) {
        System.out.println("AnotherClassInterceptor: " + s);
    }
}
