package ex;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.interceptor.Interceptors;

@Named
@Stateless
public class Hello {
    public void work() {
        work("Real work", "Yeah");
    }

    /**
     * multiple interceptors are okay
     */
    @Interceptors(HelloMethodInterceptor.class)
    public void work(String word1, String word2) {
        System.out.println("Hello: " + word1 + " " + word2);
    }
}
