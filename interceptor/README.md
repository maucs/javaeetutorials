# Interceptors

## EJB

Check the logs to see how the thing works

No need for beans.xml configuration, as this uses the **@Interceptors** annotation to do work

## CDI

To be CDI-compliant, a bit more work needs to be done, but nothing too hard

Just follow the **ex.interceptor** package, and after deploying the app, invalidate the session through JSF