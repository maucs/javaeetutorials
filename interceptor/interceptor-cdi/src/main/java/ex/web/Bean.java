package ex.web;

import ex.ejb.BusinessLogic;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;

@Named
@SessionScoped
public class Bean implements Serializable {
    @EJB
    BusinessLogic ejb;

    @PostConstruct
    public void init() {
        System.out.println("Bean: PostConstruct");
    }

    public void invalidate() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
    }
}
