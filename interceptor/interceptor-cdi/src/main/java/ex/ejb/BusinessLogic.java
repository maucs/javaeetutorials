package ex.ejb;

import ex.interceptor.Logged;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;

@Stateful
@Logged
public class BusinessLogic {
    @PostConstruct
    public void init() {
        say("PostConstruct");
    }

    public void say(String s) {
        System.out.println("BusinessLogic: " + s);
    }
}
