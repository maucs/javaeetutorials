package ex.interceptor;

import javax.annotation.PostConstruct;
import javax.annotation.Priority;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@Logged
@Interceptor
@Priority(Interceptor.Priority.APPLICATION)
public class LoggedInterceptor {
    @PostConstruct
    public Object postConstruct(InvocationContext ctx) throws Exception {
        System.out.println("LoggedInterceptor: PostConstruct for " + ctx.getMethod().getDeclaringClass().getName() + ":" + ctx.getMethod().getName());
        return ctx.proceed();
    }
}
