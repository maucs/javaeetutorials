package ex.ejb;

import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateful;
import java.util.Random;
import java.util.concurrent.Future;

@Stateful
public class SessionDelayedMessage {
    @Asynchronous
    public Future<Integer> getRandomNumber() throws InterruptedException {
        Thread.sleep(5000);
        int i = new Random().nextInt(10000);
        return new AsyncResult<>(i);
    }
}
