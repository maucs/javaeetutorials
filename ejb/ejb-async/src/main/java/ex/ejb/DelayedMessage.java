package ex.ejb;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Named;

@Named
@Stateless
public class DelayedMessage {
    @Asynchronous
    public void delayedMessage() throws Exception {
        Thread.sleep(5000);
        System.out.println("A delayed message is sent");
    }
}
