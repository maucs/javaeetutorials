package ex.web;

import ex.ejb.SessionDelayedMessage;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.concurrent.Future;

@Named
@SessionScoped
public class SessionState implements Serializable {
    @EJB
    SessionDelayedMessage ejb;
    private Future<Integer> number;
    private boolean generate;

    public Integer getNumber() throws Exception {
        return number.get();
    }

    public State getGenerate() {
        if (!generate) return State.INITIAL;
        if (number.isDone()) return State.OK;
        return State.GENERATING;
    }

    /**
     * If the method returns void, pressing the refresh button will re-send the form
     */
    public String generateRandomNumber() throws InterruptedException {
        number = ejb.getRandomNumber();
        generate = true;
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + "?faces-redirect=true";
    }

    private enum State {
        INITIAL, GENERATING, OK
    }
}
