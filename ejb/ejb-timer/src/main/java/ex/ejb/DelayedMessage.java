package ex.ejb;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.inject.Named;

@Named
@Stateless
public class DelayedMessage {
    @Resource
    TimerService timerService;

    public void startTimer() {
        timerService.createTimer(5000, "A 5s timer has started");
    }

    /**
     * argument is optional
     */
    @Timeout
    public void timeout(Timer timer) {
        System.out.println("Ring ring ring");
    }

    @Schedule(second = "*/5", minute = "*", hour = "*")
    public void automaticTimer(Timer timer) {
        System.out.println("Next call: " + timer.getNextTimeout().toString());
    }
}
