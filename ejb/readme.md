# EJB

## Singleton

Showcases **@Startup**, **@DependsOn**, and **@Lock**

## Stateful + Context

Showcases the **@AfterBegin**, **@BeforeCompletion**, and **@AfterCompletion** on transactions

Showcases the **EJBContext**

- Has a lot of EJB features, but this example only shows rollback
- Prevents any completion annotations from running, as it's not completed

I do not know how to trigger **@AfterCompletion**

## Async

**DelayedMessage** shows a simple operation that returns nothing

**SessionDelayedMessage** shows returning a value, consumed by **SessionState**

Not shown

- Future#cancel and SessionContext.wasCancelled()
- Future#get with timeout

## Timer

Just two simple timers, one manual and one automatic

## Remote

Using Payara, make sure ORB is enabled (by default it should be)

**Notes**

1. Connecting is slow
2. Getting the EJB beans is slow
3. Calls are so damn fast
4. **gf-client** is for the local application, it needs it to make ORB calls
