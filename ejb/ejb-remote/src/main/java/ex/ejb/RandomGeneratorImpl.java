package ex.ejb;

import javax.ejb.Stateless;
import java.util.Random;

@Stateless
public class RandomGeneratorImpl implements RandomGenerator {
    int randomNumber;

    @Override
    public int getRandomNumber() {
        return new Random().nextInt(1000);
    }
}
