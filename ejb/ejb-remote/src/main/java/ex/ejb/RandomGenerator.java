package ex.ejb;

import javax.ejb.Remote;

@Remote
public interface RandomGenerator {
    int getRandomNumber();
}
