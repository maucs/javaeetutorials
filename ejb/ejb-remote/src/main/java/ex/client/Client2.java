package ex.client;

import ex.ejb.RandomGenerator;

import javax.naming.Context;
import javax.naming.InitialContext;

/**
 * Much shorter, but gf-client is still needed
 */
public class Client2 {
    public static void main(String[] args) throws Exception {
        System.setProperty(Context.PROVIDER_URL, "corbaname:iiop:localhost:3700");
        InitialContext ctx = new InitialContext();

        System.out.println("RandomGenerator: Receiving...");
        RandomGenerator rg = (RandomGenerator) ctx.lookup("ex.ejb.RandomGenerator");
        System.out.println("RandomGenerator: Received!");

        System.out.println("Getting some random numbers soon...");
        Thread.sleep(2000);

        for (int i = 0; i < 100; i++)
            System.out.println(rg.getRandomNumber());

    }
}
