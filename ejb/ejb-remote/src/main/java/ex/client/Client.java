package ex.client;

import ex.ejb.RandomGenerator;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

public class Client {
    public static void main(String[] args) throws NamingException, InterruptedException {
        Properties prop = new Properties();
        prop.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.enterprise.naming.SerialInitContextFactory");
        prop.setProperty("org.omg.CORBA.ORBInitialHost", "localhost");
        prop.setProperty("org.omg.CORBA.ORBInitialPort", "3700");

        System.out.println("Context: Connecting...");
        InitialContext ctx = new InitialContext(prop);
        System.out.println("Context: Connected!");

        System.out.println("RandomGenerator: Receiving...");
        RandomGenerator rg = (RandomGenerator) ctx.lookup("ex.ejb.RandomGenerator");
        System.out.println("RandomGenerator: Received!");

        System.out.println("Getting some random numbers soon...");
        Thread.sleep(2000);

        for (int i = 0; i < 100; i++)
            System.out.println(rg.getRandomNumber());

        // closing doesn't work
    }
}
