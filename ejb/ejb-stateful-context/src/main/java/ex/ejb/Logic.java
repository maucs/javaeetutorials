package ex.ejb;

import ex.db.Human;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateful
public class Logic {
    @PersistenceContext
    EntityManager em;

    @Resource
    EJBContext ctx;

    public void doStuffs() {
        em.persist(new Human("John"));
        ctx.setRollbackOnly();
        System.out.println("Human added");
    }

    @AfterBegin
    public void afterBegin() {
        System.out.println("AfterBegin");
    }

    @BeforeCompletion
    public void beforeCompletion() {
        System.out.println("BeforeCompletion");
    }

    @AfterCompletion
    public void afterCompletion() {
        System.out.println("AfterCompletion");
    }
}
