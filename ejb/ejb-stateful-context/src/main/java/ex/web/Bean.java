package ex.web;

import ex.ejb.Logic;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@SessionScoped
public class Bean implements Serializable {
    @EJB
    Logic ejb;

    public void doStuff() throws Exception {
        ejb.doStuffs();
    }
}
