package ex;

import javax.annotation.PostConstruct;
import javax.ejb.*;

@Singleton
@Startup
@DependsOn({"One", "Two"})
@Lock(LockType.READ)
public class Three {
    @PostConstruct
    @Lock
    public void init() {
        System.out.println("Three");
    }
}
