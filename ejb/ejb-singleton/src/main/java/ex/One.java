package ex;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public class One {
    @PostConstruct
    public void init() {
        System.out.println("One");
    }
}
