# Concurrency

The Java EE tutorial [(link)](https://docs.oracle.com/javaee/7/tutorial/concurrency-utilities005.htm) shows setting up different levels of concurrency

Each class shows something different

Some interesting ones

- The EJB **@Asynchronous** shows a simplistic way of controlling thread usages

- **ManagedThreadFactory** is the proper way of creating custom resources, and is useful in session or application-scoped, Singleton or Stateful EJB 