package ex.ejb;

import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import java.util.Random;
import java.util.concurrent.Future;

@Stateless
public class SessionDelayedMessage {
    @Asynchronous
    public Future<Integer> getRandomNumber() throws InterruptedException {
        Thread.sleep(5000);
        int i = new Random().nextInt(10000);
        System.out.println(i);
        return new AsyncResult<>(i);
    }
}