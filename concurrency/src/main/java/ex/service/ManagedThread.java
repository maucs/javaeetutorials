package ex.service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedThreadFactory;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * ManagedThreadFactory useful only when an API requires usage of ThreadFactory
 */
@Named
@SessionScoped
public class ManagedThread implements Serializable {
    @Resource
    ManagedThreadFactory factory;

    ThreadPoolExecutor pool;

    @PostConstruct
    public void init() {
        pool = new ThreadPoolExecutor(1, 3, 1, TimeUnit.MINUTES, new SynchronousQueue<>(), factory);
    }

    public void submit() {
        try {
            pool.execute(() -> {
                try {
                    System.out.println("Starting");
                    Thread.sleep(4000);
                    System.out.println("Finished");
                } catch (InterruptedException e) {
                }
            });
        } catch (RejectedExecutionException e) {
            this.message(e.toString());
        }
        String msg = String.format("active count: %s/%s", pool.getActiveCount(), pool.getMaximumPoolSize());
        this.message(msg);
    }

    public void message(String msg) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Thread: " + msg));
    }
}
