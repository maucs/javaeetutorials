package ex.service;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedScheduledExecutorService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.time.LocalDateTime;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

@Named
@ApplicationScoped
public class ScheduledJobs {
    @Resource
    ManagedScheduledExecutorService executorService;

    ScheduledFuture<?> future;

    public void submitDelayedTask() {
        executorService.schedule(() -> System.out.println("Hello from delayed task"), 5, TimeUnit.SECONDS);
    }

    public void submitPeriodicTask() {
        future = executorService.scheduleAtFixedRate(() -> {
            System.out.println(LocalDateTime.now());
        }, 0, 10, TimeUnit.SECONDS);
    }

    public void stopPeriodicTask() {
        future.cancel(true);
    }
}
