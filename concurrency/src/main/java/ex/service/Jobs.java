package ex.service;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.concurrent.RejectedExecutionException;

@Named
@RequestScoped
public class Jobs {
    @Resource
    private ManagedExecutorService managedExecutorService;

    private static void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Hello");
    }

    public void submitTask() {
        try {
            managedExecutorService.submit(Jobs::run);
        } catch (RejectedExecutionException e) {
            // do something if job is rejected due to whatever reason
            // JAX-RS can return a SERVICE_UNAVAILABLE response
        }
    }

    /**
     * ManagedExecutorService is an ExecutorService
     */
    private void shutdown() {
        managedExecutorService.shutdownNow();
    }
}
