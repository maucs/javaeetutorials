package ex.service;

import ex.ejb.SessionDelayedMessage;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

@Named
@SessionScoped
public class EjbSimple implements Serializable {
    @EJB
    SessionDelayedMessage ejb;

    Future<Integer> work;

    @PostConstruct
    public void init() {
        work = CompletableFuture.completedFuture(0);
    }

    public void submit() throws InterruptedException {
        if (work.isDone()) {
            work = ejb.getRandomNumber();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("EJB: Starting work, wait for 5s"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("EJB: You cannot start a work right now"));
        }
    }
}
