package ex.service;

import javax.annotation.Resource;
import javax.transaction.*;

/**
 * Dealing with transaction in concurrency
 */
public class TxJob implements Runnable {
    @Resource
    private UserTransaction utx;

    @Override
    public void run() {
        try {
            utx.begin();
            // do stuffs..
            utx.commit();
        } catch (NotSupportedException | SystemException | HeuristicMixedException | RollbackException | HeuristicRollbackException e) {
            e.printStackTrace();
        }
    }
}
