# Deploying with Maven

While deploying using the IDE of your choice seems convenient, at the end of the day, you must deal with production deployment

One option is to use (if you're using Payara) Payara Micro, but if you need the full-server option, you can use Docker or Maven

While they can work with each other, for most deployments, Docker is overkill. You already control the deployment container (a Java EE server is like a container)

## Configuration

The Maven Cargo configuration mimics the official Java EE tutorial, where the bulk of the configurations goes to the root **pom.xml**, and config-specific goes to its respective modules

## Remote Deployments

- Deploy - `mvn clean package cargo:deploy`
- Refresh -`mvn clean package cargo:redeploy`
- Un-deploy - `mvn cargo:undeploy`

## Local Deployment 

- Start - `mvn cargo:start`
- Stop - `mvn cargo:stop`
- Start (blocks command) - `mvn cargo:run`
- Deploy, refresh, un-deploy - See **Remote Deployments**

Doesn't work with Payara, switch to GlassFish to see it work

## Links relevant to the Maven plugin

- https://codehaus-cargo.github.io
- https://codehaus-cargo.github.io/cargo/Maven2+Plugin+Getting+Started.html Command reference
- https://codehaus-cargo.github.io/cargo/Maven2+Plugin+Reference+Guide.html Configuration reference
- https://codehaus-cargo.github.io/cargo/Glassfish+4.x.html Glassfish-specific configuration