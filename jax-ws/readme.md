# JAX-WS

After running the server component, run this

```sh
  wsimport -keep -p ex.wsimport "http://localhost:8080/jax-ws/HelloService?wsdl" 
```

Copy the entire folder output to the source folder (that part is already done in this example)

And then run the main method of the class **ex.client.AppClient**

Try out

1. http://localhost:8080/jax-ws/HelloService
2. http://localhost:8080/jax-ws/HelloService?Tester