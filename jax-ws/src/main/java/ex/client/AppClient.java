package ex.client;

import ex.wsimport.HelloService;

public class AppClient {
    static HelloService service = new HelloService();

    public static void main(String[] args) {
        System.out.println(service.getHelloPort().sayHello("Hello"));
    }
}
