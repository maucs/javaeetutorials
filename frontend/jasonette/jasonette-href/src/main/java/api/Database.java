package api;

import javax.annotation.PostConstruct;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
@Startup
@Lock(LockType.READ)
public class Database {
    private Map<Integer, String> map = new ConcurrentHashMap<>();

    @PostConstruct
    public void init() {
        map.put(0, "This is a cake");
        map.put(1, "Fish in the pond");
        map.put(2, "Crazy food");
        map.put(3, "Sol");
    }

    public Map<Integer, String> getMap() {
        return Collections.unmodifiableMap(map);
    }

    public String getStringOfId(int id) {
        return map.get(id);
    }
}
