package api;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Path("jason")
@Produces(MediaType.APPLICATION_JSON)
public class CustomService {
    @EJB
    Database database;

    @Path("page2")
    @GET
    public String getPage2() throws IOException {
        return convertStreamToString("page2.json");
    }

    @Path("map")
    @GET
    public List<Entity> getEntities() {
        List<Entity> result = new ArrayList<>();
        database.getMap().forEach((integer, s) -> result.add(new Entity(integer, s)));
        return result;
    }

    @Path("map/{id}")
    @GET
    public String getSingleEntity(@PathParam("id") int id) {
        System.out.println("Searching");
        return database.getStringOfId(id);
    }

    private String convertStreamToString(String file) throws IOException {
        URL url = this.getClass().getResource("/" + file);
        java.util.Scanner s = new java.util.Scanner(url.openStream()).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    private static class Entity {
        private int id;
        private String text;

        public Entity(int id, String text) {
            this.id = id;
            this.text = text;
        }

        public int getId() {
            return id;
        }

        public String getText() {
            return text;
        }
    }
}