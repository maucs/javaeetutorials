package api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URL;

@Path("jason")
@Produces(MediaType.APPLICATION_JSON)
public class CustomService {
    @Path("templates")
    @GET
    public String getTemplates() throws IOException {
        return convertStreamToString("templates.json");
    }

    private String convertStreamToString(String file) throws IOException {
        URL url = this.getClass().getResource("/" + file);
        java.util.Scanner s = new java.util.Scanner(url.openStream()).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}