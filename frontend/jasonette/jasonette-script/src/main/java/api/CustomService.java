package api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("jason")
@Produces(MediaType.APPLICATION_JSON)
public class CustomService {
    @Path("animal")
    @GET
    public Animal getAnimal() {
        return new Animal("Mr Bob the Cat Builder", "http://i.giphy.com/OmK8lulOMQ9XO.gif", "Busy trying to fish on the not-so-latest iPhone");
    }

    private static class Animal {
        private String name, status, image;

        public Animal(String name, String image, String status) {
            this.name = name;
            this.status = status;
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public String getStatus() {
            return status;
        }

        public String getImage() {
            return image;
        }
    }
}