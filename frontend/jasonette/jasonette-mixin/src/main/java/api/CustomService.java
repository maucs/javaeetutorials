package api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

@Path("jason")
@Produces(MediaType.APPLICATION_JSON)
public class CustomService {
    @Path("view")
    @GET
    public String getView() throws IOException {
        return convertStreamToString("view.json");
    }

    @Path("template")
    @GET
    public String getTemplate() throws IOException {
        return convertStreamToString("template.json");
    }

    @Path("list")
    @GET
    public Items getList() {
        return new Items(Arrays.asList("Job", "Obsess", "Lap"));
    }

    private String convertStreamToString(String file) throws IOException {
        URL url = this.getClass().getResource("/" + file);
        java.util.Scanner s = new java.util.Scanner(url.openStream()).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    private static class Items {
        private List<String> items;

        public Items(List<String> items) {
            this.items = items;
        }

        public List<String> getItems() {
            return items;
        }
    }
}