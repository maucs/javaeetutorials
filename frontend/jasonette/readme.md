# Jasonette

All Jason files is located at **resources/main.json**

Refer to the **common** module for the base configuration

## simple

Shows body->header's title + search + menu, body->section, simple network call, and a JAX-RS service that overlaps URI path

## href

Also shows combining templates with normal components, because apparently they are not so straightforward

If json returns an integer, cast it to string, or else it won't render properly

If the return type is a string, make sure **$network.request**'s **data_type** is raw

## mixin

Shown: overused mixin. Not shown: override 

## infinite-scrolling

The **style** is there so that any new messages immediately pushes you to the bottom part of the page. Desirable for chatting-based applications

Otherwise, the cursor just stay put

Interaction with body content and footer is wonky (bottom part of body is cannibalized by the footer). Consider adding a blank space at the last part of the layout to fix it

A problem with this example is that the label can be pressed repeatedly, there is no "change button to disabled" feature

## qr

Not complete

## agent-simple

Reduces the JSON file complexity significantly. 