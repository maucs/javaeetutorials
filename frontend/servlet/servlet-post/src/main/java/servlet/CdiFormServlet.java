package servlet;

import entities.Form;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/cdi")
public class CdiFormServlet extends HttpServlet {
    @Inject
    Form form;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/cdi-form.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // Postprocess request: gather and validate submitted data and display the result in the same JSP.

        // Get and validate name.
        String name = req.getParameter("name");
        if (name == null || name.trim().isEmpty()) {
            form.setName("Please enter name");
        } else if (!name.matches("\\p{Alnum}+")) {
            form.setName("Please enter alphanumeric characters only");
        }

        // Get and validate age.
        String age = req.getParameter("age");
        if (age == null || age.trim().isEmpty()) {
            form.setAge("Please enter age");
        } else if (!age.matches("\\d+")) {
            form.setAge("Please enter digits only");
        }

        // No validation errors? Do the business job!
        if ((form.getName() == null) && (form.getAge() == null)) {
            form.setSuccess(String.format("Hello, your name is %s and your age is %s!", name, age));
        }

        req.getRequestDispatcher("/WEB-INF/cdi-form.jsp").forward(req, resp);
    }
}
