# Servlet

## Ordering

1. simple

   For security reasons (don't want users to suddenly view them), JSP files are placed in the **WEB-INF** folder 
    
   - Simple servlet with PrintWriter
   - Servlet that forwards to JSP
   - Resource loading
   - URL manipulation
   
2. post

   JSTL's URI is broken, add the JSTL dependency to fix it
   
   The CDI version of the JSP form needs to add an **elvariable** tag for development purposes. It's okay to ignore it
   
3. others

   With Java EE 8's JSON stuffs, no need to use Gson to manipulate JSON
   
   **JsonServlet** shows outputting using different types of contents
   
4. template
   
   For **genericpage.tag** + **general.jsp**
   
   - <jsp:invoke fragment="id"> and <jsp:attribute name="id">
   - <jsp:doBody> and <jsp:body>
   - <%@attribute name="header" fragment="true" %>
