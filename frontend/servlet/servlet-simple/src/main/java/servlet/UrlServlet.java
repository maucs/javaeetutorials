package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

// http://localhost:8080/servlet-simple/url?foo=one&foo=two&foo=three&id=ya
@WebServlet("/url")
public class UrlServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        writer.print("getRequestURL():      " + req.getRequestURL() + "\n");
        writer.print("getScheme():          " + req.getScheme() + "\n");
        writer.print("getServerName():      " + req.getServerName() + "\n");
        writer.print("getServerPort():      " + req.getServerPort() + "\n");
        writer.print("getRequestURI():      " + req.getRequestURI() + "\n");
        writer.print("getPathInfo():        " + req.getPathInfo() + "\n");
        writer.print("getQueryString():     " + req.getQueryString() + "\n");
        writer.print("getParameter(\"id\"):   " + req.getParameter("id") + "\n");
        writer.print("getParameter(\"none\"): " + req.getParameter("none") + "\n");

        String[] f = req.getParameterValues("foo");
        // please don't do this
        writer.print("\ngetParamaterValues(\"foo\"): " + f[0] + " " + f[1] + " " + f[2]);
    }
}
