package servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

// configurations is located at web.xml
// don't use PrintWriter, use JSP files instead
//@WebServlet(name = "ReallySimpleServlet", urlPatterns = "really-simple")
public class ReallySimpleServlet extends HttpServlet {
    // implement as many doXxx as necessary
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        PrintWriter out = resp.getWriter();
        out.println("<h1>Hello World</h1>");
    }
}
