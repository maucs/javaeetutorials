package servlet;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import utilities.Util;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// or just use JAX-RS
@WebServlet("/json")
public class JsonServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<String> list = new ArrayList<>();
        list.add("item1");
        list.add("item2");
        list.add("item3");
        String json = Util.gson.toJson(list);

        // with this, you can return any kind of content
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write(json);
    }

    // curl -v http://localhost:8080/servlet-others/json -H "Content-Type:application/json" -d '{"foo": "asd", "bar": "fds", "baz": "dqeqw"}'
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // read data from Json
        JsonObject data = new Gson().fromJson(req.getReader(), JsonObject.class);
        String foo = data.get("foo").getAsString(),
                bar = data.get("bar").getAsString(),
                baz = data.get("baz").getAsString();
        // wonder how to use this
        // String foo = req.getParameter("foo");
        // String bar = req.getParameter("bar");
        // String baz = req.getParameter("baz");

        boolean ajax = "XMLHttpRequest".equals(req.getHeader("X-Requested-With"));

        if (ajax) {
            // Handle ajax (JSON or XML) response.
        } else {
            // Handle regular (JSP) response.
        }

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write(String.format("%s %s %s\n", foo, bar, baz));
    }
}
