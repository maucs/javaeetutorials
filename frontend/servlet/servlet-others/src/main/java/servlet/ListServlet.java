package servlet;

import entities.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/list")
public class ListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Product> products = new ArrayList<>();
        req.setAttribute("products", products);
        products.add(new Product("Table", "Great piece of stand", 199));
        products.add(new Product("Banana", "The yellow doom", 2));
        products.add(new Product("Mercedes", "Almost as good...", 59000));
        products.add(new Product("Dog", "Melts your heart", 450));
        products.add(new Product("Bed", "So comfy... zzz", 1250));

        req.getRequestDispatcher("/WEB-INF/list.jsp").forward(req, resp);
    }
}
