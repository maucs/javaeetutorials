package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet
public class CookieServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // create
        Cookie cookie = new Cookie("jsid", "foobarbaz");
        resp.addCookie(cookie);

        // delete
        cookie.setMaxAge(0);
        resp.addCookie(cookie);

        // get cookies
        Cookie[] cookies = req.getCookies();
        for (Cookie c : cookies)
            System.out.printf("%s: %s%n", c.getName(), c.getValue());
    }
}
