package ex.web;

import ex.entities.Blackbox;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonParser;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

@Path("")
public class GenericServices {
    // curl -vX POST http://localhost:8080/jaxrs-consume/api/consume-string -d "foobar"
    @POST
    @Path("consume-string")
    @Produces(MediaType.APPLICATION_JSON)
    public Blackbox consumeString(String data) {
        Blackbox b = new Blackbox();
        b.setName(data);
        return b;
    }

    // curl -v http://localhost:8080/jaxrs-consume/api/consume-box -H "Content-Type:application/json" -d '{"name": "big box"}'
    @POST
    @Path("consume-box")
    @Consumes(MediaType.APPLICATION_JSON)
    public String consumeBox(Blackbox box) {
        return box.getName().toUpperCase();
    }

    // curl -v http://localhost:8080/jaxrs-consume/api/consume-stream -H "Content-Type:application/json" -d '{"name": "woof", "age": 3}'
    @POST
    @Path("consume-stream")
    @Consumes(MediaType.APPLICATION_JSON)
    public String customConsumeStream(String data) {
        String result = "";
        JsonParser parser = Json.createParser(new StringReader(data));
        while (parser.hasNext()) {
            JsonParser.Event ev = parser.next();
            switch (ev) {
                case KEY_NAME:
                    result += ev.toString() + " --> " + parser.getString() + " && ";
                    break;
                case VALUE_STRING:
                case VALUE_NUMBER:
                    result += ev.toString() + " --> " + parser.getString() + "; ";
                    break;
            }
        }
        return result;
    }

    // curl -v http://localhost:8080/jaxrs-consume/api/response-simple
    @GET
    @Path("response-simple")
    @Produces(MediaType.APPLICATION_JSON)
    public Response responseSimple() {
        JsonArray result = Json.createArrayBuilder()
                .add(Json.createObjectBuilder().add("age", 123))
                .add(Json.createObjectBuilder().add("pet", "dog"))
                .build();
        return Response.ok(result).build();
    }

    // curl -v http://localhost:8080/jaxrs-consume/api/response-stream
    @GET
    @Path("response-stream")
    @Produces(MediaType.APPLICATION_JSON)
    public Response responseStream() {
        Writer writer = new StringWriter();
        JsonGenerator gen = Json.createGenerator(writer);
        gen.writeStartObject()
                .write("firstName", "John")
                .writeStartArray("phoneNumbers")
                .writeStartObject()
                .write("type", "home")
                .write("number", 9876).writeEnd()
                .writeEnd().writeEnd();
        gen.close();
        return Response.ok(writer.toString()).build();
    }
}
