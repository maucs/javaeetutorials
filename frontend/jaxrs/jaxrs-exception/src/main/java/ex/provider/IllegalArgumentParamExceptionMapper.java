package ex.provider;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

// no example, useful when you tried to parse a "string-ly" integer
@Provider
public class IllegalArgumentParamExceptionMapper implements ExceptionMapper<IllegalArgumentException> {
    @Override
    public Response toResponse(IllegalArgumentException exception) {
        JsonObject object = Json
                .createObjectBuilder()
                .add("error", exception.toString())
                .build();
        return Response
                .status(Response.Status.BAD_REQUEST)
                .entity(object)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }
}
