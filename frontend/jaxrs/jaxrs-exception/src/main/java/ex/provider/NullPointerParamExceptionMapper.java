package ex.provider;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

// curl -v http://localhost:8080/jaxrs-exception/api/consume-box -H "Content-Type:application/json" -d '{}'
@Provider
public class NullPointerParamExceptionMapper implements ExceptionMapper<NullPointerException> {
    @Override
    public Response toResponse(NullPointerException exception) {
        JsonObject object = Json
                .createObjectBuilder()
                .add("error", exception.toString())
                .build();
        return Response
                .status(Response.Status.BAD_REQUEST)
                .entity(object)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }
}
