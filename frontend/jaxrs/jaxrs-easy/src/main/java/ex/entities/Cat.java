package ex.entities;

public class Cat implements Animal {
    @Override
    public String getType() {
        return "cat";
    }

    public String getSound() {
        return "meow";
    }

    private String getName() {
        return "james";
    }
}
