package web;

import entity.Customer;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Singleton
@Path("/customers")
public class CustomerService {
    private Map<String, Customer> db = new HashMap<>();

    @PostConstruct
    public void init() {
        db.put("James", new Customer("James", "Bob"));
    }

    @GET
    @Produces(MediaType.APPLICATION_XML)
    public Customer get(@QueryParam("name") String name) {
        return db.get(name);
    }

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    public String post(Customer customer) {
        db.put(customer.getFirstName(), customer);
        return "ok";
    }

    @Path("all")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public Collection<Customer> getAll() {
        return db.values();
    }
}
