package web;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("/echo")
public class EchoService {
    @GET
    public String get() {
        return "get";
    }

    @POST
    public String post() {
        return "post";
    }
}
