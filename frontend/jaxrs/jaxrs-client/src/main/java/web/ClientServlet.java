package web;

import entity.Customer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.Collection;

@WebServlet("/index")
public class ClientServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Client client = ClientBuilder.newClient();

        final WebTarget target = client.target("http://localhost:8080/jaxrs-client/api");

        // .path() is a shortcut, helps with creating RESTful links
        target.path("customers")
                .request()
                .post(Entity.xml(new Customer("Bobby", "Tables")));

        final Customer customer = target.path("{link}")
                .resolveTemplate("link", "customers")
                .queryParam("name", "Bobby")
                .request(MediaType.APPLICATION_XML_TYPE)
                .get(Customer.class);

        write(resp, customer.getFirstName());
        write(resp, customer.getLastName());

        // GenericType to return a list or collection
        final Collection<Customer> customers = target.path("customers")
                .path("all")
                .request(MediaType.APPLICATION_XML_TYPE)
                .get(new GenericType<Collection<Customer>>() {
                });

        write(resp, customers.size());

        client.close();
    }

    private void write(HttpServletResponse resp, Object object) throws IOException {
        resp.getWriter().write(object + "\n");
    }
}
