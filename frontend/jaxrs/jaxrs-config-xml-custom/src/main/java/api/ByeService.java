package api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("bye")
public class ByeService {
    @GET
    public String hello() {
        return "bye";
    }
}
