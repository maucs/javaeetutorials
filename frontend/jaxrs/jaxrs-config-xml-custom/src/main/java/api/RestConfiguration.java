package api;

import javax.ws.rs.core.Application;
import java.util.Collections;
import java.util.Set;

public class RestConfiguration extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        return Collections.singleton(HelloService.class);
    }
}
