# JAX-RS Tutorials

## Notes

- Payara defaults to Moxy, with Jackson as a fallback if Moxy's disabled
- To set the WAR file to deploy to root context, set up **glassfish-web.xml**

## Utility Modules

For use of dependencies (less typing)

1. config

## Tutorial Ordering

1. root - _http://localhost:8080/jaxrs-root/_

2. easy

   All public getter methods **or** public instance variables are exposed 

   - _http://localhost:8080/jaxrs-easy/api/cat_
   - _http://localhost:8080/jaxrs-easy/api/farm_
   
3. pathparam (+ Enum)

   Only getter methods is required
   
   Entering the wrong ENUM results in 404 Not Found, rather than an application error

   - _http://localhost:8080/jaxrs-pathparam/api/cat/john/10/MALE_
   
4. consume (+ response)

   When consuming Java Objects, do not add additional fields that is not part of the object itselfs
   
   Only getter methods is required, setter methods can be ommited
   
   Undeclared fields is set to **null**
   
   Refer to each of the JAX-RS methods for the **curl** command
   
5. exception

   Using **jaxrs-consume** as the base
   
6. filter (+ CORS)

   Three ways to set CORS, within method, annotations, and globally
   
   Open the html file **index.html**, change the URL, disable or enable the CORS features to see how interaction goes
   
   For class-level annotations, change **getResourceMethod** to **getResourceClass**
   
7. form

   **@BeanParam** is used, shows how to use it simply

   Webjars is used, see example for usage
   
   Go to _http://localhost:8080/jaxrs-form/_
   
8. config-xml(-*)

   Shows using **web.xml** instead of configuration to set up, and also showcases limiting JAX-RS to specific classes   
   
9. resources

   Loading from the **resources** folder, great for stuffs like **Jasonette** where the initial blob has so much JSON it's not practical to build it in the app itself   