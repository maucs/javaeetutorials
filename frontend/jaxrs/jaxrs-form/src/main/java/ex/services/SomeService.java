package ex.services;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.util.List;

@Path("")
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
@Produces(MediaType.APPLICATION_JSON)
public class SomeService {
    @POST
    @Path("auth")
    public String authenticate(@FormParam("email") String email, @FormParam("password") String password) {
        return String.format("The email [%s] has a password of [%s]", email, password);
    }

    @POST
    @Path("buy")
    public String buy(MultivaluedMap<String, String> input) {
        String name = input.getFirst("name");
        String price = input.getFirst("price");
        String color = input.getFirst("color");
        return String.format("%s (%s): $%s", name, color, price);
    }

    @POST
    @Path("sell")
    public String sell(MultivaluedMap<String, String> input) {
        List<String> names = input.get("name");
        return String.format("%s %s %s", names.get(0), names.get(1), names.get(2));
    }
}