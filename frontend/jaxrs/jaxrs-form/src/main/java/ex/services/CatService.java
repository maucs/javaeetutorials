package ex.services;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("cat")
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
@Produces(MediaType.APPLICATION_JSON)
public class CatService {
    @FormParam("name")
    public String name;

    @FormParam("age")
    public Integer age;

    @POST
    public String getCat(@BeanParam CatService cat) {
        return String.format("%s is %s years old", cat.name, cat.age);
    }
}
