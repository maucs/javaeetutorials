package ex.entities;

public class Animal {
    private String type, name;
    private Integer age;
    private Gender gender;

    public Animal(String type, String name, Integer age, Gender gender) {
        this.type = type;
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public Gender getGender() {
        return gender;
    }

    public enum Gender {
        MALE, FEMALE
    }
}
