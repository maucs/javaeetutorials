package ex.web;

import ex.entities.Animal;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("{type}/{name}/{age}/{gender}")
@Produces(MediaType.APPLICATION_JSON)
public class CatServices {
    @GET
    public Animal get(@PathParam("type") String type, @PathParam("name") String name, @PathParam("age") int age, @PathParam("gender") Animal.Gender gender) {
        return new Animal(type, name, age, gender);
    }
}
