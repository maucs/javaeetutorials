package ex.providers;

import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;

// to disable, remove this annotation
//@Provider
public class CORSFeature implements DynamicFeature {
    @Override
    public void configure(ResourceInfo resourceInfo, FeatureContext context) {
        CORS cors = resourceInfo.getResourceMethod().getAnnotation(CORS.class);
        if (cors == null) return;
        context.register(new GlobalCORSFilter());
        // the GlobalCORSFilter does not need the @Provider annotation for this to work
    }
}
