package ex.web;

import ex.providers.CORS;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("")
@Produces(MediaType.APPLICATION_JSON)
public class BoxService {

    // either way works
    @Context
    private HttpServletResponse response;

    @GET
    @Path("local")
    public String localBox(@Context HttpServletResponse resp) {
        resp.addHeader("Access-Control-Allow-Origin", "*");
        return "localocalocalocal";
    }

    @CORS
    @GET
    @Path("cors")
    public String annotationBox() {
        return "annotation";
    }
}
