package auth;

import javax.enterprise.context.ApplicationScoped;
import javax.security.enterprise.AuthenticationStatus;
import javax.security.enterprise.authentication.mechanism.http.AutoApplySession;
import javax.security.enterprise.authentication.mechanism.http.HttpAuthenticationMechanism;
import javax.security.enterprise.authentication.mechanism.http.HttpMessageContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@AutoApplySession // doesn't work without this
@ApplicationScoped
public class BasicAuthenticationMechanism implements HttpAuthenticationMechanism {
    @Override
    public AuthenticationStatus validateRequest(HttpServletRequest req, HttpServletResponse resp, HttpMessageContext ctx) {
        String user = req.getParameter("user");
        String password = req.getParameter("password");

        if (user != null || password != null)
            if (user.equals("bob") && password.equals("bob"))
                return ctx.notifyContainerAboutLogin("bob", null);
        return ctx.doNothing();
    }
}
