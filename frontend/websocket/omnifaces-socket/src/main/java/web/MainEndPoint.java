package web;

import javax.inject.Inject;
import javax.security.enterprise.SecurityContext;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@ServerEndpoint(
        value = "/echo"
        // configurator = CustomConfigurator.class
)
public class MainEndPoint {
    @Inject
    SecurityContext securityContext;

    /**
     * Closes automatically if user is not logged in
     * <p>
     * Does not stop websocket when user logs out
     */
    @OnOpen
    public void onOpen(Session session, EndpointConfig cfg) throws IOException {
        // Principal principal = (Principal) cfg.getUserProperties().get("principal");
        // cfg.getUserProperties().remove("principal"); // without this, if two browsers access the same access point, and one of them login, both can access the same data
        if (session.getUserPrincipal() == null)
            session.close(new CloseReason(CloseReason.CloseCodes.VIOLATED_POLICY, "Authentication Error"));

        // new WebSocket("ws://localhost:8080/websocket-auth/echo?foo=bar&baz=zero")
        // potentially useful for auth
        Map<String, String> paths = session.getPathParameters();
        paths.forEach((s, s2) -> System.out.println(s + ": " + s2));

        // if stacking parameters is important, use this
        // new WebSocket("ws://localhost:8080/websocket-auth/echo?foo=bar&foo=zero")
        Map<String, List<String>> requests = session.getRequestParameterMap();
        requests.forEach((s, list) -> {
            System.out.println("Big " + s);
            list.forEach(System.out::println);
        });
    }

    @OnMessage
    public void onMessage(Session session, String msg) {
        session.getAsyncRemote().sendText(session.getUserPrincipal().getName() + ": " + msg.toUpperCase());
    }

    @OnError
    public void onError(Session session, Throwable error) {
    }

    @OnClose
    public void onClose(Session session, CloseReason reason) {
        System.out.println("Closed");
    }
}
