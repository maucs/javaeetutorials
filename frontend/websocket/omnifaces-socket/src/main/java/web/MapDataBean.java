package web;

import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;

import javax.enterprise.inject.Model;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Model
public class MapDataBean {
    @Inject
    @Push
    private PushContext mapdata;

    public void send() {
        Map<String, String> data = new HashMap<>();
        data.put("title", "The Big Queen");
        data.put("desc", String.valueOf(new Random().nextInt()));
        mapdata.send(data);
    }
}
