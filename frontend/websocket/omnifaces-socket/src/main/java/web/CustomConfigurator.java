package web;

import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

/**
 * Not used, in Glassfish, the ServerEndpointConfig is a singleton, the user properties is shared among all endpoints
 */
public class CustomConfigurator extends ServerEndpointConfig.Configurator {
    @Override
    public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
        if (request.getUserPrincipal() != null)
            sec.getUserProperties().put("principal", request.getUserPrincipal());
    }
}