package web;

import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;
import org.omnifaces.util.Faces;

import javax.enterprise.inject.Model;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Model
public class ChatBean {
    @Inject
    @Push
    private PushContext all;

    @Inject
    @Push
    private PushContext same;

    @Inject
    @Push
    private PushContext one;

    public void sendAll() {
        all.send("Everyone sees this");
    }

    public void sendSame() {
        same.send("All tabs of this user sees this");
    }

    public void sendOne() {
        one.send("Only this tab sees this");
    }

    public void reset(ActionEvent event) {
        Faces.invalidateSession();
    }
}
