package web;

import javax.websocket.Endpoint;
import javax.websocket.server.ServerApplicationConfig;
import javax.websocket.server.ServerEndpointConfig;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class CustomServerApplicationConfig implements ServerApplicationConfig {
    // endpointClasses contains all classes that implements Endpoint
    @Override
    public Set<ServerEndpointConfig> getEndpointConfigs(Set<Class<? extends Endpoint>> endpointClasses) {
        Set<ServerEndpointConfig> result = new HashSet<>();
        final ServerEndpointConfig p_endpoint = ServerEndpointConfig.Builder
                .create(EchoEndPoint.class, "/echo")
                //.encoders()
                //.decoders()
                //.configurator()
                .build();
        result.add(p_endpoint);
        return result;
    }

    @Override
    public Set<Class<?>> getAnnotatedEndpointClasses(Set<Class<?>> scanned) {
        return Collections.emptySet();
    }
}
