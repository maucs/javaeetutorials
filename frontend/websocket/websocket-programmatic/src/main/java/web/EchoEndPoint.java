package web;

import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.Session;
import java.io.IOException;

public class EchoEndPoint extends Endpoint {
    @Override
    public void onOpen(Session session, EndpointConfig config) {
        System.out.println("Connected!");

        MessageHandler handler = new MessageHandler.Whole<String>() {
            @Override
            public void onMessage(String message) {
                System.out.println("received: " + message);
                try {
                    session.getBasicRemote().sendText("a message (" + message + ") is received");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        // lambda doesn't work
        session.addMessageHandler(handler);
    }
}
