package web;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.websocket.*;
import java.io.IOException;
import java.io.Serializable;
import java.net.URI;

@Named
@SessionScoped
public class ProgrammaticNormalService implements Serializable {
    WebSocketContainer container;
    Session session;

    @PostConstruct
    public void init() {
        container = ContainerProvider.getWebSocketContainer();
        try {
            session = container.connectToServer(
                    ProgrammaticWsClient.class,
                    ClientEndpointConfig.Builder.create().build(),
                    URI.create("ws://localhost:8080/websocket-client/chat"));
        } catch (DeploymentException | IOException e) {
            e.printStackTrace();
        }
    }

    public void start() throws IOException {
        session.getBasicRemote().sendText("woah");
    }

    public void stop() throws IOException {
        session.close(new CloseReason(() -> 1000, "No reason at all"));
    }
}
