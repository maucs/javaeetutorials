package web;

import javax.websocket.*;

// too lazy to deal with getUserPrincipal
@ClientEndpoint
public class WsClient {
    private ClientEndpointConfig config;
    //private String user;

    @OnOpen
    public void connected(Session session, EndpointConfig clientConfig) {
        this.config = (ClientEndpointConfig) clientConfig;
        //this.user = session.getUserPrincipal().getName();
        //System.out.println("User " + user + " connected to chatroom");
    }

    @OnMessage
    public void connected(String msg) {
        System.out.println("Message from chat server: " + msg);
    }

    @OnClose
    public void disconnected(Session session, CloseReason reason) {
        System.out.println("User disconnected as a result of " + reason.getReasonPhrase());
    }

    @OnError
    public void disconnected(Session session, Throwable error) {
        System.out.println("Error communicating with server: " + error.getMessage());
    }
}
