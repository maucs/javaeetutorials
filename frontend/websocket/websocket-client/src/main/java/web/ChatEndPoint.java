package web;

import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

@ServerEndpoint("/chat")
public class ChatEndPoint {
    private Session session;

    @OnOpen
    public void onOpen(Session session) throws IOException {
        this.session = session;
        this.session.getBasicRemote().sendText("ok");
    }

    @OnMessage
    public void getMessage(String msg) throws IOException {
        session.getBasicRemote().sendText("Echo: " + msg.toUpperCase());
    }
}
