package web;

import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(
        value = "/custom",
        decoders = CustomMessageDecoder.class,
        configurator = CustomConfigurator.class
)
public class CustomMessageEndPoint {
    @OnMessage
    public void onMessage(Session session, CustomMessage msg) {
        System.out.println("title: " + msg.getTitle());
        System.out.println("desc: " + msg.getDescription());
        session.getAsyncRemote().sendText("the title " + msg.getTitle() + " has the description " + msg.getDescription());
    }
}
