package web;

import javax.websocket.EndpointConfig;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

// http://localhost:8080/chatapp/chatrooms/currentnews
// http://localhost:8080/chatapp/chatrooms/music
// http://localhost:8080/chatapp/chatrooms/cars
// http://localhost:8080/chatapp/chatrooms/technology
@ServerEndpoint("/chatrooms/{room-name}")
public class PathParametersEndpoint {
    @OnOpen
    public void open(Session session,
                     EndpointConfig c,
                     @PathParam("room-name") String roomName) {
        // Add the client to the chat room of their choice ...
    }

    // send to all open sessions
    @OnMessage
    public void sendToAll(Session session, String msg) {
        try {
            for (Session s : session.getOpenSessions())
                if (s.isOpen())
                    s.getBasicRemote().sendText("new msg");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
