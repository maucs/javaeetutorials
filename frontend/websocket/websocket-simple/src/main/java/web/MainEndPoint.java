package web;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.nio.ByteBuffer;

@ServerEndpoint(
        value = "/echo",
        encoders = CustomMessageEncoder.class
)
public class MainEndPoint {
    @OnOpen
    public void onOpen(Session session, EndpointConfig cfg) throws IOException {
        // access it later on, in one of the @OnMessage methods
        session.getUserProperties().put("echokey", "bobthebuilder");
        System.out.println("Connected!");
        session.getBasicRemote().sendText("Connected!");
    }

    @OnMessage
    public void onMessage(Session session, String msg) throws EncodeException {
        try {
            System.out.println("Message received: " + msg);
            if (msg.equals("yes"))
                // convert by using JSON.parse in the browser
                session.getBasicRemote().sendObject(new CustomMessage("james", "the best message ever"));
            else
                session.getBasicRemote().sendText(msg.toUpperCase());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnMessage
    public void binaryMessage(Session session, ByteBuffer msg) {
        System.out.println("Binary message: " + msg.toString());
    }

    @OnMessage
    public void pongMessage(Session session, PongMessage msg) {
        System.out.println("Pong message: " +
                msg.getApplicationData().toString());
    }

    @OnError
    public void onError(Session session, Throwable error) {
    }

    @OnClose
    public void onClose(Session session, CloseReason reason) throws IOException {
        System.out.println("Destroyed!");
        session.getBasicRemote().sendText("Destroyed!");
    }
}
