package web;


import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.Session;
import java.io.InputStream;
import java.io.Reader;
import java.nio.ByteBuffer;

public class ProgrammaticReceiveEndPoint extends Endpoint {
    @Override
    public void onOpen(Session session, EndpointConfig config) {
        MessageHandler.Whole<String> complete = new MessageHandler.Whole<String>() {
            @Override
            public void onMessage(String message) {
            }
        };

        // adding messageHandler to use
        session.addMessageHandler(complete);

        MessageHandler.Partial<String> partial = new MessageHandler.Partial<String>() {
            @Override
            public void onMessage(String partialMessage, boolean last) {
            }
        };

        MessageHandler.Whole<Reader> stream = new MessageHandler.Whole<Reader>() {
            @Override
            public void onMessage(Reader message) {
            }
        };

        // binary
        MessageHandler.Whole<byte[]> binaryComplete = new MessageHandler.Whole<byte[]>() {
            @Override
            public void onMessage(byte[] message) {
            }
        };

        MessageHandler.Partial<ByteBuffer> binaryPartial = new MessageHandler.Partial<ByteBuffer>() {
            @Override
            public void onMessage(ByteBuffer partialMessage, boolean last) {
            }
        };

        MessageHandler.Whole<InputStream> binaryStream = new MessageHandler.Whole<InputStream>() {
            @Override
            public void onMessage(InputStream message) {
            }
        };


    }
}
