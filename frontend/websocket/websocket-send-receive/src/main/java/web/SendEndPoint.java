package web;

import javax.websocket.EncodeException;
import javax.websocket.RemoteEndpoint;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

// all methods is supposed to be annotated @OnMessage, and is unique
// this class showcases the ways you can send messages
//
// same for programmatic and annotation
@ServerEndpoint("/echo")
public class SendEndPoint {
    public void sync(Session session, String msg) throws IOException {
        session.getBasicRemote().sendText("message");
    }

    public void syncPartial(Session session, String msg) throws IOException {
        session.getBasicRemote().sendText("mes", false);
        session.getBasicRemote().sendText("sage", true);
    }

    public void syncStream(Session session, String msg) throws IOException {
        session.getBasicRemote().getSendWriter().write("message");
    }

    public void asyncSimple(Session session, String msg) throws ExecutionException, InterruptedException {
        final Future<Void> future = session.getAsyncRemote().sendText("message");
        future.get(); // blocks
    }

    public void asnycTimeout(Session session, String msg) throws ExecutionException, InterruptedException {
        final RemoteEndpoint.Async remote = session.getAsyncRemote();
        remote.setSendTimeout(1000); // if msg not send in 1s, don't send
        final Future<Void> message = remote.sendText("message");
        message.get(); // blocks, will timeout in 1s
    }

    public void asyncCallback(Session session, String msg) {
        session.getAsyncRemote().sendText("message", result -> {
            result.isOK();
            result.getException();
        });
    }

    // all the methods above are ways to send one type of message
    // all the methods below are the types of message you can send

    public void sendBinary(Session session, String msg) throws IOException {
        session.getBasicRemote().sendBinary(null);
    }

    public void sendJavaObject(Session session, String msg) throws IOException, EncodeException {
        session.getBasicRemote().sendObject(null);
    }
}
