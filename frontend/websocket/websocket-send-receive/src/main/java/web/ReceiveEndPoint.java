package web;

import javax.websocket.EndpointConfig;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.InputStream;
import java.io.Reader;
import java.nio.ByteBuffer;

// all methods is supposed to be annotated @OnMessage, and is unique
//
// javax.websocket.Session is optional
@ServerEndpoint("/echo")
public class ReceiveEndPoint {
    // all possible parameters for @OnMessage methods
    public void onMsgCallback(String msg, @PathParam("user") String username, Session peer, EndpointConfig config) {
    }

    public void complete(String msg) {
        System.out.println(msg);
    }

    public void partial(String msg, boolean last) {
        String chunkSeq = last ? "intermediate" : "last";
        System.out.println("Got " + chunkSeq + " chunk - " + msg);
    }

    public void stream(Reader stream) {
    }

    // binary
    public void binaryComplete(ByteBuffer buffer) {
    }

    public void binaryPartial(byte[] buffer, boolean last) {
    }

    public void binaryStream(InputStream buffer) {
    }
}
