package web;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.websocket.Session;
import java.io.IOException;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
@Lock(LockType.READ)
public class SessionScheduler {
    private Map<Session, Object> sessions = new ConcurrentHashMap<>();

    private Random random = new Random();

    @Schedule(second = "*/1", minute = "*", hour = "*")
    public void sendAdvertisement() throws IOException {
        int r = random.nextInt(1000);

        for (Session s : sessions.keySet()) {
            if (s.isOpen())
                s.getBasicRemote().sendText("This is an advertisement from company " + r);
        }
    }

    void add(Session session) {
        sessions.put(session, 0);
    }

    void remove(Session session) {
        sessions.remove(session);
    }
}
