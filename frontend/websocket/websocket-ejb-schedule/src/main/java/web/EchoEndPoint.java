package web;

import javax.ejb.EJB;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/echo")
public class EchoEndPoint {
    @EJB
    private SessionScheduler scheduler;

    @OnOpen
    public void onOpen(Session session) {
        scheduler.add(session);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        scheduler.remove(session);
    }

    @OnClose
    public void onClose(Session session) {
        scheduler.remove(session);
    }
}
