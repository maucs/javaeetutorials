package web;

import org.apache.commons.io.IOUtils;

import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;

@ServerEndpoint(
        value = "/echo"
)
public class MainEndPoint {
    @OnMessage
    public void onMessage(Session session, String msg) throws IOException {
        ByteBuffer buffer = convertStreamToString("img.png");
//        String s = Base64.getEncoder().encodeToString(buffer.array());
//        session.getAsyncRemote().sendText(s);
        session.getAsyncRemote().sendBinary(buffer);
    }

    private ByteBuffer convertStreamToString(String file) throws IOException {
        URL url = this.getClass().getResource("/" + file);
        // url.openStream().read; // Java 9 solves this with .readAllBytes
        byte[] bytes = IOUtils.toByteArray(url);
        return ByteBuffer.wrap(bytes);
    }
}
