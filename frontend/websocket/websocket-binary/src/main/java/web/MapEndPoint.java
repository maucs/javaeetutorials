package web;

import org.apache.commons.io.IOUtils;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.net.URL;
import java.util.Base64;

@ServerEndpoint(
        value = "/map-echo"
)
public class MapEndPoint {
    @OnMessage
    public void onMessage(Session session, String msg) throws IOException {
        Data data = new Data("img", Base64.getEncoder().encodeToString(convertStreamToString("img.png")));
        Jsonb jsonb = JsonbBuilder.create();
        session.getAsyncRemote().sendText(jsonb.toJson(data));
    }

    private byte[] convertStreamToString(String file) throws IOException {
        URL url = this.getClass().getResource("/" + file);
        return IOUtils.toByteArray(url);
    }

    public static class Data {
        private String id;
        private String data;

        public Data(String id, String data) {
            this.id = id;
            this.data = data;
        }

        public String getId() {
            return id;
        }

        public String getData() {
            return data;
        }
    }
}
