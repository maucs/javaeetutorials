package web;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

public class CustomMessageEncoder implements Encoder.Text<CustomMessage> {
    @Override
    public String encode(CustomMessage object) throws EncodeException {
        return String.format("{\"title\": \"%s\", \"desc\": \"%s\"}", object.getTitle(), object.getDescription());
    }

    @Override
    public void init(EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }
}
