package web;

import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;
import java.io.StringReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CustomMessageDecoder implements Decoder.Text<CustomMessage> {
    private Map<String, String> messageMap;

    @Override
    public CustomMessage decode(String s) throws DecodeException {
        return new CustomMessage(messageMap.get("title"), messageMap.get("desc"));
    }

    @Override
    public boolean willDecode(String s) {
        System.out.println("Decoding " + s);
        messageMap = new HashMap<>();
        JsonParser parser = Json.createParser(new StringReader(s));
        while (parser.hasNext()) {
            if (parser.next() == JsonParser.Event.KEY_NAME) {
                String key = parser.getString();
                parser.next();
                String value = parser.getString();
                messageMap.put(key, value);
            }
        }
        return messageMap.keySet().containsAll(Arrays.asList("title", "desc"));
    }

    @Override
    public void init(EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }
}
