package web;

import org.omnifaces.util.Faces;

import javax.enterprise.inject.Model;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;

@Model
public class Main {
    public String logout() throws ServletException {
        Faces.invalidateSession();
        Faces.logout();
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + "?faces-redirect=true";
    }
}
