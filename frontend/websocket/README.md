# WebSocket

## Auth

Security API must use **@AutoApplySession**, otherwise WebSocket will not pick up the **UserPrincipal**

## Omnifaces Socket

The scope feature provided by **o:socket** is a poor man's auth

## Binary

If you want to send a data with metadata, Base64 the binary data, then send it as Json