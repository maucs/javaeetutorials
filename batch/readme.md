# Batch

## Modules

1. batch-simple

   The file has to be inserted manually. On first use, click on the "Start" button, and place an input file named **input.txt** with text of your choice into the directory outputted in the console log. The output file is located in the same directory as the input file 