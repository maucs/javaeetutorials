package chunk;

import javax.batch.api.chunk.ItemReader;
import javax.batch.runtime.context.JobContext;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Serializable;

@Dependent
@Named
public class MyReader implements ItemReader {
    @Inject
    JobContext jobContext;
    private MyCheckpoint myCheckpoint;
    private BufferedReader reader;

    @Override
    public void open(Serializable checkpoint) throws Exception {
        String workingDir = System.getProperty("user.dir");
        System.out.println("Current working directory : " + workingDir);

        // if there was already a checkpoint, use that
        // otherwise, init a new one
        if (checkpoint == null)
            myCheckpoint = new MyCheckpoint();
        else
            myCheckpoint = (MyCheckpoint) checkpoint;

        String filename = jobContext.getProperties()
                .getProperty("input_file");
        reader = new BufferedReader(new FileReader(filename));

        // move the buffer so that it's in line with the checkpoint
        for (long i = 0; i < myCheckpoint.getLineNum(); i++)
            reader.readLine();
    }

    @Override
    public void close() throws Exception {
        reader.close();
    }

    @Override
    public Object readItem() throws Exception {
        return reader.readLine();
    }

    @Override
    public Serializable checkpointInfo() throws Exception {
        return null;
    }
}
