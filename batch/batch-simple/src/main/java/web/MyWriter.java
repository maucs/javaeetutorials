package web;

import chunk.MyCheckpoint;

import javax.batch.api.chunk.ItemWriter;
import javax.batch.runtime.context.JobContext;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.Serializable;
import java.util.List;

@Dependent
@Named
public class MyWriter implements ItemWriter {
    private BufferedWriter writer;

    @Inject
    private JobContext jobContext;

    @Override
    public void open(Serializable checkpoint) throws Exception {
        String filename = jobContext.getProperties()
                .getProperty("output_file");
        writer = new BufferedWriter(new FileWriter(filename, (checkpoint != null)));
    }

    @Override
    public void close() throws Exception {
        writer.close();
    }

    @Override
    public void writeItems(List<Object> items) throws Exception {
        for (int i = 0; i < items.size(); i++) {
            String line = (String) items.get(i);
            writer.write(line);
            writer.newLine();
        }
    }

    @Override
    public Serializable checkpointInfo() throws Exception {
        return new MyCheckpoint();
    }
}
