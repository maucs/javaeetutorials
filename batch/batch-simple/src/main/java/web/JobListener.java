package web;

import javax.enterprise.context.Dependent;
import javax.inject.Named;
import java.util.logging.Level;
import java.util.logging.Logger;

@Dependent
@Named
public class JobListener implements javax.batch.api.listener.JobListener {
    private static final Logger logger = Logger.getLogger("InfoJobListener");

    @Override
    public void beforeJob() throws Exception {
        logger.log(Level.INFO, "The job is starting");
    }

    @Override
    public void afterJob() throws Exception {
        logger.log(Level.INFO, "The job has finished.");
    }
}
