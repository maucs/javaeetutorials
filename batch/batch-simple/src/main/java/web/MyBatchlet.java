package web;

import javax.batch.api.Batchlet;
import javax.batch.runtime.context.JobContext;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;

@Dependent
@Named
public class MyBatchlet implements Batchlet {
    @Inject
    private JobContext context;

    @Override
    public String process() throws Exception {
        String filename = context.getProperties()
                .getProperty("output_file");
        System.out.println("" + (new File(filename)).length());
        return "COMPLETED";
    }

    @Override
    public void stop() throws Exception {

    }
}
