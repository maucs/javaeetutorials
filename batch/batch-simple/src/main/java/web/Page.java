package web;

import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.JobExecution;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named
public class Page {
    final JobOperator jobOperator = BatchRuntime.getJobOperator();

    public void start() {
        // returns a job id, type long
        jobOperator.start("simplejob", null);
    }

    // not used, this is how you get the current status of the submitted job
    public void get(long id) {
        JobExecution jobExec = jobOperator.getJobExecution(id);
        String status = jobExec.getBatchStatus().toString();
    }
}
