package ex.config;

import ex.ejb.EjbMain;
import ex.entity.Autobots;

import javax.annotation.PostConstruct;
import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@DataSourceDefinition(
        name = "java:global/MyApp/MyDataSource",
        className = "org.postgresql.ds.PGSimpleDataSource",
        portNumber = 5432,
        databaseName = "mau",
        user = "mau"
)
@Singleton
@Startup
public class DataSource {
    @EJB
    EjbMain ejb;

    @PostConstruct
    public void init() {
        // showcases UUID autogeneration
        ejb.addEntity(new Autobots());
        ejb.addEntity(new Autobots());
        ejb.addEntity(new Autobots());
        ejb.addEntity(new Autobots());
    }
}
