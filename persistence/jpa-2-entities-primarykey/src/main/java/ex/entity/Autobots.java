package ex.entity;

import org.eclipse.persistence.annotations.UuidGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@UuidGenerator(name = "HUMAN_ID_GEN")
public class Autobots {
    @Id
    @GeneratedValue(generator = "HUMAN_ID_GEN")
    public String id;
}
