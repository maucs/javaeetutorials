package ex.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Furniture {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    public Long id;

    public String name, colour;

    public Furniture() {
    }

    public Furniture(String name, String colour) {
        this.name = name;
        this.colour = colour;
    }
}
