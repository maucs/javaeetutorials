package ex.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Human {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    public String name;
    public Integer age;
    public Long networth;

    public Human() {
    }

    public Human(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

}
