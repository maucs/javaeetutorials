package ex.ejb;

import ex.entity.Human;

import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

@Stateful
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class HumanBean {
    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    EntityManager em;

    public void create(Human h) {
        em.persist(h);
    }

    /**
     * Does nothing
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void save() {
    }
}
