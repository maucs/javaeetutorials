package ex.web;

import ex.ejb.HumanBean;
import ex.entity.Human;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@SessionScoped
public class FrontPage implements Serializable {
    @EJB
    HumanBean bean;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void add() {
        bean.create(new Human(name));
    }

    public void save() {
        bean.save();
    }
}
