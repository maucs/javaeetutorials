# Advance: Gateway

By taking advantage of the **Extended** context and the **TransactionAttributeType**, one can hold all commits as long as possible until the end

This minimizes database calls, merges, persist, etc

The example shown here is minimal, one should implement the entity manager's refresh, find, etc