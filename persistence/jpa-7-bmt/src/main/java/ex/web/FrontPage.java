package ex.web;

import ex.ejb.HumanBean;
import ex.entity.Human;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import java.io.Serializable;

@Named
@SessionScoped
public class FrontPage implements Serializable {
    @EJB
    HumanBean bean;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void add() throws NotSupportedException, SystemException {
        bean.create(new Human(name));
    }

    public void save() throws Exception {
        bean.save();
    }

    public void rollback() throws Exception {
        bean.rollback();
    }
}
