package ex.ejb;

import ex.entity.Human;

import javax.annotation.Resource;
import javax.ejb.Stateful;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

@Stateful
@TransactionManagement(TransactionManagementType.BEAN)
public class HumanBean {
    @Resource
    UserTransaction utx;

    @PersistenceContext
    EntityManager em;

    public void create(Human h) throws SystemException, NotSupportedException {
        utx.setTransactionTimeout(5);
        utx.begin();
        em.persist(h);
    }

    public void save() throws Exception {
        utx.commit();
    }

    public void rollback() throws Exception {
        utx.rollback();
    }
}
