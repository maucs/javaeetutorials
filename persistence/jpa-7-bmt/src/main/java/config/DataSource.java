package config;

import ex.entity.Human;

import javax.annotation.PostConstruct;
import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@DataSourceDefinition(
        name = "java:global/MyApp/MyDataSource",
        className = "org.postgresql.ds.PGSimpleDataSource",
        portNumber = 5432,
        databaseName = "mau",
        user = "mau"
)
@Singleton
@Startup
public class DataSource {
    @PersistenceContext
    EntityManager em;

    @PostConstruct
    void init() {
        em.persist(new Human("James"));
        em.persist(new Human("Lisa"));
        em.persist(new Human("Norf"));
        em.persist(new Human("Larry"));
        em.persist(new Human("Pomp"));
    }
}
