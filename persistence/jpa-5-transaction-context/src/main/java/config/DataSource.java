package config;

import ex.ejb.EjbMain;
import ex.entity.Human;

import javax.annotation.PostConstruct;
import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@DataSourceDefinition(
        name = "java:global/MyApp/MyDataSource",
        className = "org.postgresql.ds.PGSimpleDataSource",
        portNumber = 5432,
        databaseName = "mau",
        user = "mau"
)
@Singleton
@Startup
public class DataSource {
    @EJB
    EjbMain ejb;

    @PostConstruct
    void init() {
        ejb.add(new Human("James"));
        ejb.add(new Human("Urgu"));
        ejb.add(new Human("Leah"));
        ejb.add(new Human("Pane"));
    }
}
