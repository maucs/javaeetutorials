package ex.web;

import ex.ejb.EjbMain;
import ex.entity.Human;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.List;


@Named
@RequestScoped
public class Bean {
    @EJB
    EjbMain ejb;

    public List<Human> getHumans() {
        return ejb.getAllHumans();
    }

    public void testRollback() {
        ejb.testRollback();
    }
}
