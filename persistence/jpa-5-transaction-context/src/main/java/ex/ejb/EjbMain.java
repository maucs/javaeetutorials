package ex.ejb;

import ex.entity.Human;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class EjbMain {
    @PersistenceContext
    EntityManager em;

    @Resource
    EJBContext ctx;

    public void add(Human h) {
        em.persist(h);
    }

    public void testRollback() {
        Human human = em.find(Human.class, 1);
        human.setName("HOLOLOLOLOLBOGOGOGOGOG");
        human.setName("YES NO WHY I CHANGE THIS");

        em.persist(new Human("Jojo"));
        em.flush();

        em.persist(new Human("Kiki"));
        em.persist(new Human("Lolo"));
        ctx.setRollbackOnly();
        System.out.println("yes");
        em.persist(new Human("Hohoho Merry Christmas"));
        human.setName("I PROMISE THIS IS THE LAST");
        em.flush();
    }

    public List<Human> getAllHumans() {
        return em.createQuery("SELECT h FROM Human h", Human.class).getResultList();
    }
}
