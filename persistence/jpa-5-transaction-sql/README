Objectives
==========

Using JDBC, raw SQL, and a 3rd party library (myBatis) to operate on entities, bypassing JPA's Entity Manager (but still respecting EJB transactions)

1. EJB transactions on SQL Database calls
2. Interaction of @Transactional and @TransactionAttribute

Note
====

JDBC and MyBatis respects EJB transactions as long the original DataSource is used

- JDBC: Just inject the resource using @Resource
- MyBatis: In the configuration, set transactionManager to MANAGED, and dataSource to JNDI. Refer to resources/mybatis.xml

Additional Requirements
=======================

The ability to read database transaction logs
  - PostgreSQL: https://stackoverflow.com/a/722236