package ex.ejb;

import ex.entity.Animal;
import ex.entity.AnimalMapper;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Stateless
public class EjbDs {
    @Resource(lookup = "java:global/MyApp/MyDataSource")
    DataSource ds;

    @Inject
    AnimalMapper mapper;

    public void createTable() {
        try (Connection conn = ds.getConnection()) {
            conn.prepareStatement("DROP TABLE IF EXISTS animal").execute();
            conn.prepareStatement("CREATE TABLE animal (id BIGSERIAL PRIMARY KEY, name VARCHAR(255), sound VARCHAR(255))").execute();
        } catch (SQLException e) {
        }
    }

    /**
     * Using preparedStatement, adding one by one
     */
    public void addData() {
        try (Connection conn = ds.getConnection()) {
            PreparedStatement st = conn.prepareStatement("INSERT INTO animal (name, sound) VALUES (?, ?)");
            st.setString(1, "Dog");
            st.setString(2, "Woof");
            st.execute();
        } catch (SQLException e) {
        }
    }

    /**
     * Batch add
     */
    public void addData(Map<String, String> input) {
        try (Connection conn = ds.getConnection()) {
            PreparedStatement st = conn.prepareStatement("INSERT INTO animal (name, sound) VALUES (?, ?)");
            for (Map.Entry<String, String> pair : input.entrySet()) {
                st.setString(1, pair.getKey());
                st.setString(2, pair.getValue());
                st.addBatch();
            }
            st.executeBatch();
        } catch (SQLException e) {
        }
    }

    /**
     * Using MyBatis's Mapper, refer to resources/mybatis.xml for the full configuration
     */
    public List<Animal> getAllAnimals() {
        try (Connection conn = ds.getConnection()) {
            conn.prepareStatement("INSERT INTO animal (name, sound) VALUES ('fish', 'squabble')").execute();
        } catch (SQLException e) {
        }
        mapper.insert("Cryfish", "Crying");
        return mapper.getAllAnimals();
    }

    public void insertAnimal(String name, String sound) {
        mapper.insert(name, sound);
    }
}
