package ex.web;

import ex.ejb.EjbDs;
import ex.entity.Animal;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Here, two separate methods, to test transactional features
 */
@RequestScoped
@Named("bean")
public class AnimalJsf {
    @EJB
    EjbDs ds;

    public List<Animal> getAnimals() {
        return ds.getAllAnimals();
    }

    public void insertNoTx() {
        ds.insertAnimal("Rabbit", "Bounce");
        ds.insertAnimal("Dragon", "Fire");
        ds.insertAnimal("Rapper", "Many words");
    }

    @Transactional
    public void insertTx() {
        ds.insertAnimal("Rabbit", "Bounce");
        ds.insertAnimal("Dragon", "Fire");
        ds.insertAnimal("Rapper", "Many words");
    }
}
