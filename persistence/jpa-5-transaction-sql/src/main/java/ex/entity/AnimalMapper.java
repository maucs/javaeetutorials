package ex.entity;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.mybatis.cdi.Mapper;

import java.util.List;

/**
 * The Mapper interface eases SQL development and usage
 * <p>
 * Ignore the error, it compiles fine
 */
@Mapper
public interface AnimalMapper {
    @Select("SELECT * FROM animal")
    List<Animal> getAllAnimals();

    @Insert("INSERT INTO animal (name, sound) VALUES (#{param1}, #{param2})")
    void insert(String name, String sound);
    //void insert(@Param("name") String name, @Param("sound") String sound);
    // the above works, just change the param values
}
