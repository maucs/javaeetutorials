package ex.config;

import ex.ejb.EjbDs;
import ex.ejb.EjbMain;

import javax.annotation.PostConstruct;
import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.util.HashMap;
import java.util.Map;

@DataSourceDefinition(
        name = "java:global/MyApp/MyDataSource",
        className = "org.postgresql.ds.PGSimpleDataSource",
        portNumber = 5432,
        databaseName = "mau",
        user = "mau"
)
@Singleton
@Startup
public class DataSource {
    @EJB
    EjbMain ejb;

    @EJB
    EjbDs ds;

    @PostConstruct
    public void init() {
        ds.createTable();
        ds.addData();

        Map<String, String> input = new HashMap<>();
        input.put("Cat", "Meow");
        input.put("Lion", "Roar");
        input.put("Rabbit", "???");

        ds.addData(input);
    }
}
