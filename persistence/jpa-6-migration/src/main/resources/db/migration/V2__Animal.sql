CREATE TABLE ANIMAL (
  ID    BIGINT NOT NULL,
  NAME  VARCHAR(255),
  SOUND VARCHAR(255),
  PRIMARY KEY (ID)
);