CREATE TABLE human (
  id   INTEGER NOT NULL,
  age  INTEGER,
  name VARCHAR(255),
  PRIMARY KEY (id)
);
-- for @GeneratedValue
CREATE TABLE sequence (
  seq_name  VARCHAR(50) NOT NULL,
  seq_count DECIMAL(38),
  PRIMARY KEY (SEQ_NAME)
);
INSERT INTO sequence (seq_name, seq_count) VALUES ('SEQ_GEN', 0)