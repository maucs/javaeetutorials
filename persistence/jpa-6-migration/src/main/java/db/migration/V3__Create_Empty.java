package db.migration;

import org.flywaydb.core.api.migration.jdbc.JdbcMigration;

import java.sql.Connection;

public class V3__Create_Empty implements JdbcMigration {
    @Override
    public void migrate(Connection connection) throws Exception {
        connection.prepareStatement("CREATE TABLE empty (id INTEGER NOT NULL)").execute();
    }
}
