package ex.config;

import ex.ejb.EjbMain;
import ex.entity.Animal;
import ex.entity.Human;
import org.flywaydb.core.Flyway;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@DataSourceDefinition(
        name = "java:global/MyApp/MyDataSource",
        className = "org.postgresql.ds.PGSimpleDataSource",
        portNumber = 5432,
        databaseName = "mau",
        user = "mau"
)
@Singleton
@Startup
public class DataSource {
    @Resource(lookup = "java:global/MyApp/MyDataSource")
    javax.sql.DataSource ds;

    @EJB
    EjbMain ejb;

    @PostConstruct
    public void init() {
        initFlywaydb();
        initData();
    }

    /**
     * This method deletes whatever tables that were marked, and then the migration process begins
     */
    private void initFlywaydb() {
        Flyway flyway = new Flyway();
        flyway.setDataSource(ds);
        flyway.clean();
        flyway.migrate();
    }

    private void initData() {
        ejb.addEntity(new Human("James", 20));
        ejb.addEntity(new Human("Jerthom", 23));
        ejb.addEntity(new Human("Kinky", 19));

        ejb.addEntity(new Animal("Chicken", "Cuckoo"));
        ejb.addEntity(new Animal("Dog", "Woof"));
        ejb.addEntity(new Animal("Cat", "Meow"));
    }
}
