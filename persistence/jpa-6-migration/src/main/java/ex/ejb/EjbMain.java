package ex.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class EjbMain {
    @PersistenceContext
    EntityManager em;

    /*
    Shortcut, don't do this, cast the right object type
     */
    public void addEntity(Object object) {
        em.persist(object);
    }
}
