package ex2.config;

import ex.ejb.EjbMain;
import ex2.entity.Animal;
import ex2.entity.Entity2;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Startup;
import java.util.*;

//@Singleton
@Startup
public class Config {
    @EJB
    EjbMain ejb;

    @PostConstruct
    public void init() {
        Animal animal1 = new Animal("Elephant", "Hngggg");
        Animal animal2 = new Animal("Lion", "Roar");

        Set<Animal> set1 = new HashSet<>();
        set1.add(animal1);
        set1.add(animal2);
        Set<Animal> set2 = new HashSet<>();
        set2.add(animal1);
        set2.add(animal2);
        Map<Integer, Animal> map = new HashMap<>();
        map.put(100, animal1);
        map.put(200, animal2);
        Entity2 e1 = new Entity2(new Date(), new Animal("Dog", "Woof"), animal1, set1, set2, map, null);

        Entity2 e2 = new Entity2();
        e2.date = new Date();
        e2.animal = new Animal("Cat", "Meow");
        e2.manyAnimals = animal1;
        e2.setAnimals = new HashSet<>();
        e2.setAnimals.add(animal1);

        ejb.addEntity(e1);
        ejb.addEntity(e2);
        ejb.addEntity(animal2);
    }
}
