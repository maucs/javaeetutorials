package ex2.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/*
This shows how uni-directional entity2 relationship works, and generating temporal values
 */
//@Entity
public class Entity2 {
    @Id
    @GeneratedValue
    public Long id;

    @Temporal(TemporalType.TIMESTAMP)
    public Date date;

    /*
    Both this and @ManyToOne creates a foreign-key column
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "animal_primary_key")
    public Animal animal;

    @ManyToOne(cascade = CascadeType.ALL)
    public Animal manyAnimals;

    /*
    Creates a new table {this_class}_{other_class}

    Default
    Table name: {this_class}_{other_class}
    Foreign_key Column 1: {this_class}_id
    Foreign_key Column 2: {other_class}_id
    Foreign key: fk_{table_name}_{column_name}_key (there are two foreign keys)
     */
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "entity2_set_animals",
            joinColumns = @JoinColumn(name = "entity2_primary_key"),
            inverseJoinColumns = @JoinColumn(name = "animal_primary_key", referencedColumnName = "id"),
            foreignKey = @ForeignKey(name = "fk_for_entity2_with_animal_for_primary_key_of_entity2"),
            inverseForeignKey = @ForeignKey(name = "fk_for_entity2_with_animal_for_primary_key_of_animal")
    )
    public Set<Animal> setAnimals;

    /*
    Since top and bottom variables have the same target class, their default table name is the same, and this can cause issues
     */
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "entity2_zoo_animals")
    public Set<Animal> zooAnimals;

    /*
    This will create two new columns in the map value class's table
     */
    @OneToMany(cascade = CascadeType.ALL)
    @MapKeyJoinColumn(name = "id_of_animals")
    @JoinColumn(name = "target_of_animals")
    public Map<Integer, Animal> idAnimals;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "map_of_integer_and_animals",
            joinColumns = @JoinColumn(name = "entity2_pkey"),
            inverseJoinColumns = @JoinColumn(name = "map_value_is_animal_pkey")
    )
    @MapKeyColumn(name = "map_key")
    public Map<Integer, Animal> mapAnimals;

    public Entity2() {
    }

    public Entity2(Date date, Animal animal, Animal manyAnimals, Set<Animal> setAnimals, Set<Animal> zooAnimals, Map<Integer, Animal> idAnimals, Map<Integer, Animal> mapAnimals) {
        this.date = date;
        this.animal = animal;
        this.manyAnimals = manyAnimals;
        this.setAnimals = setAnimals;
        this.zooAnimals = zooAnimals;
        this.idAnimals = idAnimals;
        this.mapAnimals = mapAnimals;
    }
}
