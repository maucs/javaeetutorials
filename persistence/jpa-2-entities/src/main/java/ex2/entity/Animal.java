package ex2.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

//@Entity
public class Animal {
    @Id
    @GeneratedValue
    public Integer id;

    public String name, sound;

    public Animal() {
    }

    public Animal(String name, String sound) {
        this.name = name;
        this.sound = sound;
    }
}

