package ex3.entity;

import javax.persistence.*;
import java.util.List;

//@Entity
public class Employee {
    @Id
    @GeneratedValue
    public Integer id;
    public String job;
    public Integer salary;
    /*
    Makes sure each employee is linked to a unique company
     */
    @OneToOne
    @JoinColumn(
            name = "the_id_of_the_company",
            unique = true
            //referencedColumnName = "company_id"
    )
    public Entity3 company;
    @ManyToOne
    public Entity3 companyDepartment;
    /*
    joinColumns refer to this (owner) class
    inverseJoinColums refer to the other class (in this case, Entity3)
     */
    @ManyToMany
    @JoinTable(
            joinColumns = @JoinColumn(name = "employee_pkey")
    )
    public List<Entity3> investments;

    public Employee() {
    }

    public Employee(String job, Integer salary) {
        this.job = job;
        this.salary = salary;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }
}
