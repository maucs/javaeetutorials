package ex3.entity;

import javax.persistence.*;
import java.util.List;

/*
Bi-directional stuffs

This is not an owner class
The details is found in the other side of the database

This entity2 does not do cascading, to show how much work it takes
 */
//@Entity
public class Entity3 {
    @Id
    @GeneratedValue
    public Integer id;

    @OneToOne(mappedBy = "company")
    public Employee owner;

    @OneToMany(mappedBy = "companyDepartment")
    public List<Employee> workers;

    @ManyToMany(mappedBy = "investments")
    public List<Employee> investors;

    public Integer getId() {
        return id;
    }

    public Employee getEmployee() {
        return owner;
    }

    public void setEmployee(Employee employee) {
        this.owner = employee;
    }
}
