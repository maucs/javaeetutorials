package ex3.config;

import ex.ejb.EjbMain;
import ex3.entity.Employee;
import ex3.entity.Entity3;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Startup;

//@Singleton
@Startup
public class Config {
    @EJB
    EjbMain ejb;

    @PostConstruct
    public void init() {
        Employee e1 = new Employee("Janitor", 3000);
        Employee e2 = new Employee("Programmer", 30000);

        Entity3 entity = new Entity3();

        e1.company = entity;
//        entity2.employee = e1;

        ejb.addEntity(entity);
        ejb.addEntity(e1);
        ejb.addEntity(e2);
    }
}
