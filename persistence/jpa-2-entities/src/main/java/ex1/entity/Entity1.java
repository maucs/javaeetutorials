package ex1.entity;

import javax.persistence.*;
import java.util.Map;
import java.util.Set;

/*
This entities shows how table and columns are generated

It has no dependencies with other entities

It is disabled because it creates so much noisy table names, enable it
to see how it works
 */
//@Entity
@Table(name = "entity1")
public class Entity1 {
    @Id
    @GeneratedValue
    @Column(name = "entity1_id")
    public Integer id;

    /*
    Column name defaults to {variable_name}
    */
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "username")
    public String name;

    /*
    ElementCollection creates a new table

    Defaults
    Table name: {entity2}_{variable_name}
    Foreign key: {entity2}_{entity_id_column} (under @CollectionTable's joinColumn)
    Column name: {variable_name}
     */
    @ElementCollection
    @CollectionTable(name = "string_sets", joinColumns = @JoinColumn(name = "entity1_pkey"))
    @Column(name = "set_values")
    public Set<String> set_of_strings;

    /*
    Defaults
    Key's column name: {variable_name}_key
     */
    @ElementCollection
    @MapKeyColumn(name = "map_key")
    public Map<String, String> map_string_string;
}
