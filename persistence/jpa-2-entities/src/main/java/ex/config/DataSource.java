package ex.config;

import javax.annotation.sql.DataSourceDefinition;

@DataSourceDefinition(
        name = "java:global/MyApp/MyDataSource",
        className = "org.postgresql.ds.PGSimpleDataSource",
        portNumber = 5432,
        databaseName = "mau",
        //url = "jdbc:postgresql://localhost:5432/mau",
        user = "mau",
        password = ""
)
public class DataSource {
}
