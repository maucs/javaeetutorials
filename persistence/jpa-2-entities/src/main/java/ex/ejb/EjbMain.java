package ex.ejb;

import ex.entity2.Human;
import ex3.entity.Entity3;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class EjbMain {
    @PersistenceContext
    EntityManager em;

    public void addHuman(Human h) {
        em.persist(h);
    }

    public List<Human> getAllHumans() {
        return em.createQuery("SELECT h FROM Human h", Human.class).getResultList();
    }

    /*
    Shortcut, don't do this, cast the right object type
     */
    public void addEntity(Object object) {
        em.persist(object);
    }

    public List<Entity3> getEntity3() {
        return em.createQuery("SELECT e FROM Entity3 e", Entity3.class).getResultList();
    }
}
