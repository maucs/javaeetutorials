package ex.web;

import ex.ejb.EjbMain;
import ex3.entity.Entity3;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.List;

@Named("bean")
@RequestScoped
public class Bean {
    @EJB
    EjbMain ejb;

    public List<Entity3> getEntity3() {
        return ejb.getEntity3();
    }
}
