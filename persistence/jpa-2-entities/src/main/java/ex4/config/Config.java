package ex4.config;

import ex.ejb.EjbMain;
import ex4.entity.Entity4;
import ex4.entity.LastSeen;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public class Config {
    @EJB
    EjbMain ejb;

    @PostConstruct
    public void init() {
        Entity4 e1 = new Entity4();
        e1.lastSeen = new LastSeen("Toilet", "Working Dress");
        e1.withWho = new LastSeen("Shopping Mall", "Sexy");

        Entity4 e2 = new Entity4();
        e2.lastSeen = new LastSeen("Home", "Pyjamas");
        e2.withWho = new LastSeen("Home", "Sexy Time");

        ejb.addEntity(e1);
        ejb.addEntity(e2);
    }
}
