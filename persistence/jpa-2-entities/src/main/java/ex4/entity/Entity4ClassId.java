package ex4.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(Entity4Id.class)
public class Entity4ClassId {
    @Column(name = "student_id")
    @Id
    Integer studentId;

    @Column(name = "food_id")
    @Id
    Long foodId;
}
