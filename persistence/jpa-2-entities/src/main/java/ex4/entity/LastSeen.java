package ex4.entity;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class LastSeen implements Serializable {
    public String location, dress;

    public LastSeen(String location, String dress) {
        this.location = location;
        this.dress = dress;
    }

    public LastSeen() {
    }
}
