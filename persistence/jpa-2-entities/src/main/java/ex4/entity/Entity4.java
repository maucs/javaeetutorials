package ex4.entity;

import org.eclipse.persistence.annotations.UuidGenerator;

import javax.persistence.*;

/*
Embeddable, Version, and UUID generation
 */
@Entity
@UuidGenerator(name = "HUMAN_ID_GEN")
public class Entity4 {
    @Id
    @GeneratedValue(generator = "HUMAN_ID_GEN")
    public String id;

    @Version
    public Long version;

    /*
    Annotation optional
     */
    @Embedded
    public LastSeen lastSeen;

    /*
    If the embeddable class has a relationship, use @AssociationOverrides

    https://en.wikibooks.org/wiki/Java_Persistence/Embeddables
     */
    @AttributeOverrides({
            @AttributeOverride(name = "location", column = @Column(name = "location_to_cheat")),
            @AttributeOverride(name = "dress", column = @Column(name = "what_sinful_dress"))
    })
    public LastSeen withWho;
}
