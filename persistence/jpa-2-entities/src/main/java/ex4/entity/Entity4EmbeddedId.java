package ex4.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class Entity4EmbeddedId {
    //@AttributeOverride
    @EmbeddedId
    public LastSeen id;
}
