package ex.config;

import ex.ejb.EjbMain;
import ex.entity2.Company;
import ex.entity2.Human;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Startup
@Singleton
public class InitData {
    // here, all operations have to be done manually, adding a company does not automatically merge or persist humans

    @EJB
    EjbMain ejb;

    @PostConstruct
    public void init() {
        Company c1 = new Company("Apple", "A great and like-able company");
        Company c2 = new Company("Microsoft", "Bill Gates is the best");

        Human h1 = new Human("John", 20, c1);
        Human h2 = new Human("Jane", 32, c2);
        Human h3 = new Human("Mary", 19, c1);
        Human h4 = new Human("Martha", 23, c2);
        Human h5 = new Human("Zach", 20, c1);
        Human h6 = new Human("Yah", 20, c1);

        ejb.addEntity(c1);
        ejb.addEntity(c2);

        ejb.addEntity(h1);
        ejb.addEntity(h2);
        ejb.addEntity(h3);
        ejb.addEntity(h4);
        ejb.addEntity(h5);
        ejb.addEntity(h6);
    }
}
