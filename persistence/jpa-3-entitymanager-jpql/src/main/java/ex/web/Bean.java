package ex.web;

import ex.ejb.EjbEm;
import ex.ejb.EjbJpql;
import ex.entity2.AnalyticsSalary;
import ex.entity2.Company;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.List;

@Named("bean")
@RequestScoped
public class Bean {
    @EJB
    EjbEm em;

    @EJB
    EjbJpql jpql;

    AnalyticsSalary apple;
    AnalyticsSalary microsoft;

    @PostConstruct
    public void init() {
        apple = jpql.getAverageSalary("Apple");
        microsoft = jpql.getAverageSalary("Microsoft");
    }

    /*
    Try each one to see the effects of SQL production
     */
    public List<Company> getCompanies() {
        return jpql.getAllCompaniesJoinFetch();
//        return jpql.getAllCompanies();
//        return jpql.getAllCompanies(true);
//        return jpql.getAllCompanies(false);
    }

    public AnalyticsSalary getApple() {
        return apple;
    }

    public AnalyticsSalary getMicrosoft() {
        return microsoft;
    }

    public void update() {
//        jpql.updateAgeApple(); // n+1 select issue
        em.updateApple(); // no n+1 issue
    }

    public void delete() {
        jpql.deleteApple();
    }

}
