package ex.entity2;

public class AnalyticsSalary {
    public String name;
    public Double integer;

    public AnalyticsSalary(String name, Double integer) {
        this.name = name;
        this.integer = integer;
    }

    public String getName() {
        return name;
    }

    public Double getInteger() {
        return integer;
    }
}
