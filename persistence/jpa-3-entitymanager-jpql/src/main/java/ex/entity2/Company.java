package ex.entity2;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@NamedQuery(
        name = "Company.getAll",
        query = "SELECT c FROM Company c"
        // do entity2 graph here, just to show the difference
)
public class Company {
    @Id
    public String name;

    public String description;

    /*
    There is an attribute 'orphanRemoval', which is a more aggressive version of CascadeType.REMOVE

    'CascadeType.REMOVE' only removes the child when this entity2 is removed, not when the worker element is set to null, or the relationship is removed

    'orphanRemoval' removes the child when the element relationship dissapears
     */
    @OneToMany(mappedBy = "workplace", fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    public Set<Human> workers;

    public Company() {
    }

    public Company(String name, String description) {
        this.name = name;
        this.description = description;
        this.workers = new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public Set<Human> getWorkers() {
        return workers;
    }
}
