package ex.entity2;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Human {
    @Id
    @GeneratedValue
    public Long id;
    public String name;
    public Integer age;
    @ManyToOne
    public Company workplace;

    public Human() {
    }

    public Human(String name, Integer age, Company workplace) {
        this.name = name;
        this.age = age;
        this.workplace = workplace;
        workplace.workers.add(this);
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }
}
