package ex.ejb;

import ex.entity2.AnalyticsSalary;
import ex.entity2.Company;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Random;

/**
 * Shown here are JPQL parameters in method
 * <p>
 * Named parameters: {@link #getAverageSalary(String)}
 * Ordinal parameters: {@link #updateAgeApple()}
 */
@Stateless
public class EjbJpql {
    @PersistenceContext
    EntityManager em;

    public List<Company> getAllCompanies() {
        em.getEntityManagerFactory().getCache().evictAll();
        return em.createNamedQuery("Company.getAll", Company.class).getResultList();
    }

    /*
    Without the keyword DISTINCT, multiple same copies will be produced

    SELECT t1.NAME, t0.ID, t0.AGE, t0.NAME, t0.WORKPLACE_NAME FROM HUMAN t0, COMPANY t1 WHERE (t0.WORKPLACE_NAME = t1.NAME)

    With keyword DISTINCT

    SELECT DISTINCT t1.NAME, t0.ID, t0.AGE, t0.NAME, t0.WORKPLACE_NAME FROM HUMAN t0, COMPANY t1 WHERE (t0.WORKPLACE_NAME = t1.NAME)
     */
    public List<Company> getAllCompaniesJoinFetch() {
        return em.createQuery("SELECT DISTINCT c FROM Company c JOIN FETCH c.workers", Company.class).getResultList();
    }

    /* An example of JPQL functions and returning them in a typesafe manner */
    public AnalyticsSalary getAverageSalary(String company) {
        TypedQuery<AnalyticsSalary> query = em.createQuery("SELECT new ex.entity2.AnalyticsSalary(h.workplace.name, AVG(h.age)) FROM Human h WHERE h.workplace.name = :w GROUP BY h.workplace", AnalyticsSalary.class);
        query.setParameter("w", company);
        return query.getSingleResult();
    }

    /*
    This caused n+1 select, I suspect it's because the cache was invalidated
     */
    public void updateAgeApple() {
        Random random = new Random();
        Query q = em.createQuery("UPDATE Human SET age = ?1 WHERE workplace.name = 'Apple'");
        q.setParameter(1, random.nextInt(100) + 100).executeUpdate();
    }

    /*
    Using JPQL on tables with foreign-keys managed by JPA, is like a landmine. You must update the foreign-key data or else errors are thrown
     */
    public void deleteApple() {
        //em.createQuery("DELETE FROM Company WHERE name = 'Apple'").executeUpdate();
        Company company = em.find(Company.class, "Apple");
        em.remove(company);
    }
}
