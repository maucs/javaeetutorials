package ex.ejb;

import ex.entity2.Company;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Random;

@Stateless
public class EjbEm {
    @PersistenceContext
    EntityManager em;

    public void updateApple() {
        Random random = new Random();
        int age = random.nextInt(100) + 100;
        Company company = em.find(Company.class, "Apple");
        em.clear();
        company.workers.forEach(human -> human.age = age);
        em.merge(company);
    }

    public void deleteApple() {
        Company company = em.find(Company.class, "Apple");
        em.remove(company);
    }
}
