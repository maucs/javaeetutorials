package ex.web;

import ex.ejb.EjbOne;
import ex.entity2.Human;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.List;

@Named("human")
@RequestScoped
public class HumanBean {
    List<Human> animals;

    @EJB
    EjbOne ejb1;

    public List<Human> getHumansJpa() {
        return ejb1.getAllHumans();
    }
}
