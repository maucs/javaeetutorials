package ex;

import ex.ejb.EjbOne;
import ex.ejb.EjbTwo;
import ex.entity2.Human;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public class Config {
    @EJB
    EjbOne ejb1;

    @EJB
    EjbTwo ejb2;

    @PostConstruct
    public void init() {
        ejb1.addHuman(new Human("James", 20));
        ejb2.addHuman(new Human("Janica", 22));
        ejb2.addHuman(new Human("Alex", 19));
    }
}
