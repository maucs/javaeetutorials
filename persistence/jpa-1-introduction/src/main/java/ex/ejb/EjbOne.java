package ex.ejb;

import ex.entity2.Human;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class EjbOne {
    @PersistenceContext
    EntityManager em;

    public void addHuman(Human h) {
        em.persist(h);
    }

    public List<Human> getAllHumans() {
        return em.createQuery("SELECT h FROM Human h", Human.class).getResultList();
    }
}
