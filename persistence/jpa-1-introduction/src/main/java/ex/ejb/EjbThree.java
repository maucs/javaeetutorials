package ex.ejb;

import ex.entity2.Human;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/*
Somehow, DefaultDataSource don't work in Payara
 */
//@Stateless
public class EjbThree {
    // @Resource(lookup = "java:comp/DefaultDataSource")
    DataSource ds;

    public List<Human> getAllHumans() {
        List<Human> result = new ArrayList<>();
        try (Connection conn = ds.getConnection()) {
            Statement statement = conn.createStatement();
            ResultSet res = statement.executeQuery("SELECT id, name, age FROM human");
            while (res.next()) {
                result.add(new Human(res.getString("name"), res.getInt("age")));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return result;
    }
}
