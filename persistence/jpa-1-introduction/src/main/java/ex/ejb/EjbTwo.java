package ex.ejb;

import ex.entity2.Human;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class EjbTwo {
    @PersistenceContext(unitName = "app-unit")
    EntityManager em;

    public void addHuman(Human h) {
        em.persist(h);
    }
}
