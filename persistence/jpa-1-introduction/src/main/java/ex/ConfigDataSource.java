package ex;

import javax.annotation.sql.DataSourceDefinition;

@DataSourceDefinition(
        name = "java:global/MyApp/MyDataSource",
        className = "org.apache.derby.jdbc.EmbeddedDataSource",
        url = "memory:myDB",
        // user = "user",
        // password = "password",
        databaseName = "memory:myDB"
        // serverName = "localhost"
)
public class ConfigDataSource {
}
