package ex.ejb;

import ex.entity.Building;

import javax.ejb.Stateless;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Subgraph;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Stateless
public class EjbJpql {
    @PersistenceContext
    EntityManager em;

    /*
    TODO fix SQL result
    fetchgraph: SELECT NAME FROM COMPANY
    loadgraph: SELECT NAME, DESCRIPTION FROM COMPANY
     */
    public List<Building> getAllBuildings(boolean loadFurnituresAndHumansOnly) {
        EntityGraph<?> graph = em.getEntityGraph("graph.Building.getBuildings");
        String hint = loadFurnituresAndHumansOnly ? "javax.persistence.fetchgraph" : "javax.persistence.loadgraph";
        return em.createQuery("SELECT b FROM Building b", Building.class)
                .setHint(hint, graph)
                .getResultList();
    }

    /*
    Create Entity Graphs manually, using it with Entity Manager operations
     */
    public void createEntityGraph() {
        EntityGraph<Building> graph = em.createEntityGraph(Building.class);
        graph.addAttributeNodes("name");
        Subgraph<Object> subgraph = graph.addSubgraph("furnitures");
        subgraph.addAttributeNodes("material");

        Map<String, Object> hints = new HashMap<>();
        hints.put("javax.persistence.loadgraph", graph);

        em.find(Building.class, "name here", hints);
    }
}
