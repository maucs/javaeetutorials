package ex.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class EjbEclipselink {
    @PersistenceContext
    EntityManager em;

    /**
     * join-fetch is similar to entitygraph, jpql fetch join. it will create one big query
     * batch is different, it will collect the results only when required, but it's smarter than a n+1 select
     */
    public void getAllBuildings() {
        em.createQuery("SELECT b FROM Building b")
                .setHint("eclipselink.read-only", "TRUE")
                .setHint("eclipselink.join-fetch", "b.furnitures")
//                .setHint("eclipselink.batch", "b.furnitures")
                .setHint("eclipselink.batch", "b.humans")
                .getResultList();
    }
}
