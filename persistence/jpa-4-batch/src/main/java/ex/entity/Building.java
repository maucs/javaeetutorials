package ex.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@NamedEntityGraph(
        name = "graph.Building.getBuildings",
        attributeNodes = {
                @NamedAttributeNode("furnitures"),
                @NamedAttributeNode(value = "humans", subgraph = "humans")
        },
        subgraphs = @NamedSubgraph(
                name = "humans",
                attributeNodes = @NamedAttributeNode("gender")
        )

)
public class Building {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    public String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<Furniture> furnitures;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<Human> humans;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Furniture> getFurnitures() {
        return furnitures;
    }

    public List<Human> getHumans() {
        return humans;
    }
}
