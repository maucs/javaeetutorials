package ex.web;

import ex.ejb.EjbStateful;
import ex.entity.Human;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@SessionScoped
public class BeanStateful implements Serializable {
    @EJB
    EjbStateful stateful;

    Human human;

    @PostConstruct
    public void init() {
        human = stateful.getHuman();
    }

    /*
    Together with PersistenceContextType.EXTENDED, the entity is still managed even after exiting EJB method
     */
    public boolean getManaged() {
        return stateful.isManaged(human);
    }
}
