package ex.web;

import ex.ejb.EjbStateless;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.transaction.Transactional;


@Named("bean")
@RequestScoped
public class Bean {
    @EJB
    EjbStateless stateless;

    /**
     * The two methods show how transaction works
     * {@link #runTransactional()} executes one tx
     * {@link #runNoTransactional()} executes two tx
     */
    @Transactional
    public void runTransactional() {
        stateless.change("ONE");
        stateless.change("TWO");
    }

    public void runNoTransactional() {
        stateless.change("ONE");
        stateless.change("TWO");
    }

    // Throws an exception
    public void noChange() {
        stateless.noCommit();
    }

    public void runTwoTransactions() {
        stateless.runTwoTransactions();
    }

    public void exceptionAndCommit() throws Exception {
        stateless.throwsAndCommits();
    }

    public void exceptionNoCommit() {
        stateless.throwsNoCommit();
    }
}
