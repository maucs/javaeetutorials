package ex.ejb;

import ex.entity.Human;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class EjbMain {
    @PersistenceContext
    EntityManager em;

    public void add(Human h) {
        em.persist(h);
    }
}
