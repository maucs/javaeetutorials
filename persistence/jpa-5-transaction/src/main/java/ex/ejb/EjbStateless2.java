package ex.ejb;

import ex.entity.Human;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class EjbStateless2 {
    @PersistenceContext
    EntityManager em;

    /**
     * Refer to {@link EjbStateless#runTwoTransactions()}
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void change() {
        em.find(Human.class, 1).setDescription("Changed from EjbStateless2 in a new transaction");
    }
}
