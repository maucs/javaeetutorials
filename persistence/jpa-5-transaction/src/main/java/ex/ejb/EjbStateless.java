package ex.ejb;

import ex.entity.Human;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class EjbStateless {
    @PersistenceContext
    EntityManager em;

    @EJB
    EjbStateless2 ejbStateless2;

    public void change(String prefix) {
        List<Human> humans = em.createQuery("SELECT h FROM Human h", Human.class).getResultList();
        em.clear();
        humans.forEach(human -> {
            human.description = prefix + " changed from stateless";
            em.merge(human);
        });
    }

    // Throws an Exception
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public void noCommit() {
        List<Human> humans = em.createQuery("SELECT h FROM Human h", Human.class).getResultList();
        em.clear();
        humans.forEach(human -> {
            human.description = "Changed from Stateless#noCommit";
            em.merge(human);
        });
    }

    /*
    This runs two transactions, and the calls are ordered based on which transaction finishes first

    Additionally, use setter methods, changing instances variable directly DOES NOT work
     */
    public void runTwoTransactions() {
        Human human = em.find(Human.class, 2);
        human.setDescription("Changed from EjbStateless in the current transaction");

        ejbStateless2.change();

        Human human2 = em.find(Human.class, 3);
        human2.setDescription("Changed from EjbStateless in the current transaction");
    }

    /*
    Both methods interacts with exception and rollbacks

    As long it is thrown, rollback (if applicable) happens, even if it was caught
     */
    public void throwsAndCommits() throws Exception {
        em.persist(new Human("Exception Child"));
        em.persist(new Human("Special Child"));
        throw new Exception();
        //throw new BusinessException(); // this will rollback, see class for more details
    }

    public void throwsNoCommit() {
        em.persist(new Human("Greatest Person Of All Time"));
        throw new RuntimeException();
    }
}
