package ex.ejb;

import ex.entity.Human;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

@Stateful
public class EjbStateful {
    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    EntityManager em;

    public Human getHuman() {
        return em.find(Human.class, 1);
    }

    public boolean isManaged(Human h) {
        return em.contains(h);
    }
}
