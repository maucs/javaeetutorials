package ex.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Human {
    @Id
    @GeneratedValue
    public Integer id;
    public String name, description;

    public Human() {
    }

    public Human(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
