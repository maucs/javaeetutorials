package ex.ejb;


import ex.entity.Human;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;

@Stateless
public class EjbMain {
    @PersistenceContext
    EntityManager em;

    public void add(Object o) {
        em.persist(o);
    }

    /**
     * As long as you use this entity (rather than re-finding a new one), the lock stays even after detach
     */
    public Human getHuman(Long id) {
//        return em.find(Human.class, id, LockModeType.OPTIMISTIC);
        return em.find(Human.class, id, LockModeType.NONE);
    }

    /**
     * Run a pessimistic lock operation, will do a database lock
     */
    public void voidPessimisticLock() {
        em.find(Human.class, 2L, LockModeType.PESSIMISTIC_READ);
    }


    public void dirtyUpdate(Human human) {
        em.createQuery("UPDATE Human SET name = 'COWABUNGA' WHERE id = ?1")
                .setParameter(1, human.getId())
                .executeUpdate();
        human.setName("BOGOBOGOBOGOBOGOBOGO");
        em.merge(human);
    }

    public Human update(Human human, String name) {
        human.setName(name);
        return em.merge(human);
    }
}
