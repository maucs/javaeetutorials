package ex.ejb;

import ex.entity.Human;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

/**
 * Test the effects of @Version on long-running persistence context
 */
@Stateful
public class EjbStateful {
    /**
     * This causes a SQL UPDATE to always happen
     */
    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    EntityManager em;

    public Human getHuman(long l, LockModeType lock) {
        return em.find(Human.class, l, lock);
    }

    public boolean isManaged(Object o) {
        return em.contains(o);
    }

    /**
     * Sneakily modifies, bypassing the Persistence Context
     */
    public void ninjaEdit(int i) {
        em.createQuery("UPDATE Human SET name = 'THE ADVENTURES OF JOJO' WHERE id = ?1")
                .setParameter(1, i)
                .executeUpdate();
    }

    public void normalEdit(Human h) {
        h.setName("The One True King: Stannis");
    }
}
