package ex.config;

import ex.ejb.EjbMain;
import ex.entity.Human;

import javax.annotation.PostConstruct;
import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@DataSourceDefinition(
        name = "java:global/MyApp/MyDataSource",
        className = "org.postgresql.ds.PGSimpleDataSource",
        portNumber = 5432,
        databaseName = "mau",
        user = "mau")
@Singleton
@Startup
public class DataSource {
    @EJB
    EjbMain ejb;

    @PostConstruct
    public void init() {
        ejb.add(new Human("James"));
        ejb.add(new Human("Dork"));
        ejb.add(new Human("Tik"));
        ejb.add(new Human("Dop"));
        ejb.add(new Human("Harry"));
        ejb.add(new Human("Lassy"));
        ejb.add(new Human("Sally"));
        ejb.add(new Human("Veronica"));
    }
}
