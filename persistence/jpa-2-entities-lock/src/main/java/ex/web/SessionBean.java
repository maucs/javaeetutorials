package ex.web;

import ex.ejb.EjbStateful;
import ex.entity.Human;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.LockModeType;
import java.io.Serializable;
import java.util.Random;

@Named
@SessionScoped
public class SessionBean implements Serializable {
    @EJB
    EjbStateful ejb;

    Integer random;
    Human subject;
    Human untouched;

    @PostConstruct
    public void init() {
        subject = ejb.getHuman(1L, LockModeType.OPTIMISTIC);
        untouched = ejb.getHuman(2L, LockModeType.OPTIMISTIC);
        Random r = new Random();
        random = r.nextInt(1000);
    }

    /**
     * This is so that the page is refreshed, immediately showing new random values
     * <p>
     * Without a refresh, the page still shows old data even though it is gone
     */
    public String destroy() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return FacesContext.getCurrentInstance().getViewRoot().getViewId() + "?faces-redirect=true";
    }

    public void ninjaEdit(int i) {
        ejb.ninjaEdit(i);
    }

    public void normalEdit() {
        ejb.normalEdit(subject);
    }

    public Integer getRandom() {
        return random;
    }

    /**
     * The entity is guaranteed to be managed as long the Persistence Context is extended
     */
    public boolean isHumanManaged() {
        return ejb.isManaged(subject);
    }

    public Human getSubject() {
        return subject;
    }
}
