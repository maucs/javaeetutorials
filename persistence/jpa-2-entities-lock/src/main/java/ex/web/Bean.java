package ex.web;

import ex.ejb.EjbMain;
import ex.entity.Human;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named("bean")
@RequestScoped
public class Bean {
    @EJB
    EjbMain ejb;

    Human oneHuman;

    @PostConstruct
    public void init() {
        oneHuman = ejb.getHuman(1L);
    }

    public Human getOneHuman() {
        return oneHuman;
    }

    public void doVoidPessimisticLock() {
        ejb.voidPessimisticLock();
    }

    /**
     * Does a dirty-update before merging it
     */
    public void doUpdateDirty() {
        ejb.dirtyUpdate(oneHuman);
    }

    public void doUpdateNameOne() {
        oneHuman = ejb.update(oneHuman, "Name One");
    }

    public void doUpdateNameTwo() {
        oneHuman = ejb.update(oneHuman, "Name Two");
    }
}
