package ex.web;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

@Named
@RequestScoped
public class BrowserBean {
    @Inject
    JMSContext context;

    @Resource(lookup = "java:comp/jms/queue")
    Queue queue;

    public List<String> getList() throws JMSException {
        Enumeration e = context.createBrowser(queue).getEnumeration();
        List<String> result = new ArrayList<>();

        while (e.hasMoreElements()) {
            Message m = (Message) e.nextElement();
            result.add(m.getBody(String.class));
        }

        return result;
    }
}
