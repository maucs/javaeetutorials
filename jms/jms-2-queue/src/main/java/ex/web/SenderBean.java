package ex.web;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.JMSContext;
import javax.jms.Queue;

@Named
@RequestScoped
public class SenderBean {
    @Inject
    JMSContext context;

    @Resource(lookup = "java:comp/jms/sync")
    Queue syncQueue;

    @Resource(lookup = "java:comp/jms/async")
    Queue asyncQueue;

    String message;

    public void sendMessage() {
        context.createProducer().send(syncQueue, message);

        // message for JSF
        FacesMessage facesMessage =
                new FacesMessage("Sent sync message: " + message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }

    public void sendAsyncMessage() {
        context.createProducer().send(asyncQueue, message);

        // message for JSF
        FacesMessage facesMessage =
                new FacesMessage("Sent async message: " + message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
