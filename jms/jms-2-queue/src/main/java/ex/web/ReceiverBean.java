package ex.web;


import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.JMSContext;
import javax.jms.Queue;

@Named
@RequestScoped
public class ReceiverBean {
    @Inject
    JMSContext context;

    @Resource(lookup = "java:comp/jms/sync")
    Queue queue;

    public void receiveMessage() {
        String s = context.createConsumer(queue).receiveBody(String.class, 1000);

        String msg = (s != null)
                ? "Reading message: " + s
                : "No message received after 1 second";
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(msg));
    }
}
