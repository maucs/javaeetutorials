package ex.config;

import javax.jms.JMSDestinationDefinition;
import javax.jms.JMSDestinationDefinitions;

@JMSDestinationDefinitions({
        @JMSDestinationDefinition(
                name = "java:comp/jms/sync",
                interfaceName = "javax.jms.Queue",
                destinationName = "SyncQueue"),
        @JMSDestinationDefinition(
                name = "java:comp/jms/async",
                interfaceName = "javax.jms.Queue",
                destinationName = "AsyncQueue")
})
public class ConfigQueue {
}
