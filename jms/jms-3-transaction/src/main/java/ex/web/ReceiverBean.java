package ex.web;


import ex.ejb.MessageEjb;
import ex.exception.BusinessException;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.JMSContext;
import javax.jms.Queue;

@Named
@RequestScoped
public class ReceiverBean {
    @Inject
    JMSContext context;

    @Resource(lookup = "java:comp/jms/queue")
    Queue queue;

    @EJB
    MessageEjb ejb;

    public void receiveMessage() {
        String s = context.createConsumer(queue).receiveBody(String.class, 1000);

        String msg = (s != null)
                ? "Reading message: " + s
                : "No message received after 1 second";
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(msg));
    }

    public void receiveMessageRollback() {
        ejb.exceptionOne();
    }

    public void receiveMessageExceptionRollback() {
        try {
            ejb.exceptionTwo();
        } catch (BusinessException e) {
        }
    }
}
