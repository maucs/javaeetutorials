package ex.ejb;

import ex.exception.BusinessException;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;

@Stateless
public class MessageEjb {
    @Resource
    SessionContext sc;

    @Inject
    JMSContext context;

    @Resource(lookup = "java:comp/jms/queue")
    Queue queue;

    public void exceptionOne() {
        context.createConsumer(queue).receiveBody(String.class, 1000);
        sc.setRollbackOnly();
    }

    public void exceptionTwo() throws BusinessException {
        context.createConsumer(queue).receiveBody(String.class, 1000);
        throw new BusinessException();
    }
}
