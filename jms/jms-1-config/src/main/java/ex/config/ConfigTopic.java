package ex.config;

import javax.jms.JMSDestinationDefinition;

/**
 * Configures a topic
 */
@JMSDestinationDefinition(
        name = "java:comp/jms/topic",
        interfaceName = "javax.jms.Topic",
        destinationName = "WebappTopic"
)
public class ConfigTopic {
}
