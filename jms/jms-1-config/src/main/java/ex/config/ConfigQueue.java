package ex.config;

import javax.jms.JMSDestinationDefinition;

/**
 * Configures a queue
 */
@JMSDestinationDefinition(
        name = "java:comp/jms/queue",
        interfaceName = "javax.jms.Queue",
        destinationName = "WebappQueue"
)
public class ConfigQueue {
}
