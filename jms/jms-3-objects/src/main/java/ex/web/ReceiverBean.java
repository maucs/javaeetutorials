package ex.web;

import ex.entity.Human;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.JMSContext;
import javax.jms.Queue;

@Named
@RequestScoped
public class ReceiverBean {
    @Inject
    JMSContext context;

    @Resource(lookup = "java:comp/jms/queue")
    Queue queue;

    public void receiveMessage() {
        Human h = context.createConsumer(queue).receiveBody(Human.class, 1000);

        String msg = (h != null)
                ? "Reading message: " + h.name
                : "No message received after 1 second";
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(msg));
    }
}
