package ex.web;

import ex.entity.Human;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.JMSContext;
import javax.jms.JMSDestinationDefinition;
import javax.jms.JMSException;
import javax.jms.Queue;

@JMSDestinationDefinition(
        name = "java:comp/jms/queue",
        interfaceName = "javax.jms.Queue",
        destinationName = "WebappQueue"
)
@Named
@RequestScoped
public class SenderBean {
    @Inject
    JMSContext context;

    @Resource(lookup = "java:comp/jms/queue")
    Queue queue;

    String message;

    public void sendMessage() throws JMSException {
        context.createProducer().send(queue, new Human(message));

        // message for JSF
        FacesMessage facesMessage =
                new FacesMessage("Sent sync message: " + message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
