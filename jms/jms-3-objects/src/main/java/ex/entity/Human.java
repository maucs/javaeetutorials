package ex.entity;

import java.io.Serializable;

public class Human implements Serializable {
    public String name;

    public Human(String name) {
        this.name = name;
    }
}
