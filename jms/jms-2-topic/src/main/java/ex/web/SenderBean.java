package ex.web;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.JMSContext;
import javax.jms.Topic;

@Named
@RequestScoped
public class SenderBean {
    @Inject
    JMSContext context;

    @Resource(lookup = "java:comp/jms/topic")
    Topic topic;

    String message;

    public void sendMessage() {
        context.createProducer().send(topic, message);

        // message for JSF
        FacesMessage facesMessage =
                new FacesMessage("Sent message: " + this.message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
