# Java Message Service

## 1 - Configuration

Shows creation of destinations (queue, topic) using annotations

## 2 - Simple Queue

Shows how to send and receives messages sync and asynchronously

The asynchronous (message-driven bean)'s important config list are there

## 2 - Simple Topic

# Note

Some of the modules depends on the config module, do check the Maven POM (or the lack of ex.config package)

## 3 - Advanced Topics

- Browser
- Objects
- Selectors
- Temporary Destination

    Uses queue, but it can work on topic the same way. Just showcases how to create and destroy a temporary destination
        
- Transactions