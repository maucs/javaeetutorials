package ex.config;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.TemporaryQueue;

@ApplicationScoped
public class Config {
    @Inject
    JMSContext context;

    TemporaryQueue queue;

    @PostConstruct
    public void init() {
        queue = context.createTemporaryQueue();
    }

    @PreDestroy
    public void destroy() throws JMSException {
        queue.delete();
    }

}
