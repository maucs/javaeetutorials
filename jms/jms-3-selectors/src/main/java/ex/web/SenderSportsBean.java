package ex.web;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.*;

@JMSDestinationDefinition(
        name = "java:comp/jms/queue",
        interfaceName = "javax.jms.Queue",
        destinationName = "WebappQueue"
)
@Named
@RequestScoped
public class SenderSportsBean {
    @Inject
    JMSContext context;

    @Resource(lookup = "java:comp/jms/queue")
    Queue queue;

    String message;

    public void sendMessage() throws JMSException {
        TextMessage msg = context.createTextMessage();
        msg.setText(message);
        msg.setStringProperty("Type", "Sports");
        context.createProducer().send(queue, msg);

        // message for JSF
        FacesMessage facesMessage =
                new FacesMessage("Sent sync message: " + message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
