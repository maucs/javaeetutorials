package ex.web;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.JMSContext;
import javax.jms.Queue;

@Named
@RequestScoped
public class ReceiverBean {
    @Inject
    JMSContext context;

    @Resource(lookup = "java:comp/jms/queue")
    Queue queue;

    public void receiveSportsMessage() {
        String s = context.createConsumer(queue, "Type = 'Sports'").receiveBody(String.class, 1000);

        String msg = (s != null)
                ? "Reading message: " + s
                : "No message received after 1 second";
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(msg));
    }

    public void receiveVehicleMessage() {
        String s = context.createConsumer(queue, "Type = 'Vehicle'").receiveBody(String.class, 1000);

        String msg = (s != null)
                ? "Reading message: " + s
                : "No message received after 1 second";
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(msg));
    }
}
