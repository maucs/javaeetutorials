package web;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "BasicServlet", value = "/basic")
public class BasicServlet extends HttpServlet {
    @Inject
    private Generator generator;

    @Inject
    @Big
    private Generator bigGenerator;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().write(String.valueOf(generator.getNumber() + "\n"));
        resp.getWriter().write(String.valueOf(bigGenerator.getNumber() + "\n"));
    }
}
