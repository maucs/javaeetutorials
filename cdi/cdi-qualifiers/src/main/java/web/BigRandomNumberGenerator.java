package web;

import javax.enterprise.context.RequestScoped;
import java.util.Random;

@Big
@RequestScoped
public class BigRandomNumberGenerator extends RandomNumberGenerator {
    public BigRandomNumberGenerator() {
        number = new Random().nextInt(1000000);
    }
}
