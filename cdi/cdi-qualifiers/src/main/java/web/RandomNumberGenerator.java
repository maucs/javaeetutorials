package web;

import javax.enterprise.context.RequestScoped;
import java.util.Random;

@RequestScoped
public class RandomNumberGenerator implements Generator {
    protected int number = new Random().nextInt(1000);

    public int getNumber() {
        return number;
    }
}
