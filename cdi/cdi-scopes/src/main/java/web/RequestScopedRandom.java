package web;

import javax.enterprise.context.RequestScoped;
import java.util.Random;

@RequestScoped
public class RequestScopedRandom implements ScopedRandom {
    int random = new Random().nextInt();

    public int get() {
        return random;
    }
}
