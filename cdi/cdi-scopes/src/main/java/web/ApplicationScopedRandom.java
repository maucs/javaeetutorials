package web;

import javax.enterprise.context.ApplicationScoped;
import java.util.Random;

@ApplicationScoped
public class ApplicationScopedRandom implements ScopedRandom {
    int random = new Random().nextInt();

    public int get() {
        return random;
    }
}