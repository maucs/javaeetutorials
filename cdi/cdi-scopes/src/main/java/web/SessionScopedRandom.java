package web;

import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Random;

@SessionScoped
public class SessionScopedRandom implements Serializable, ScopedRandom {
    int random = new Random().nextInt();

    public int get() {
        return random;
    }
}
