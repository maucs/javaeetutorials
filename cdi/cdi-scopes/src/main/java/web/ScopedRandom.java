package web;

public interface ScopedRandom {
    int get();
}
