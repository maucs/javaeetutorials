package web;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "BasicServlet", value = "/basic")
public class BasicServlet extends HttpServlet {
    @Inject
    private ApplicationScopedRandom applicationScopedRandom;

    @Inject
    private SessionScopedRandom sessionScopedRandom;

    @Inject
    private RequestScopedRandom requestScopedRandom;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        write(applicationScopedRandom, resp);
        write(sessionScopedRandom, resp);
        write(requestScopedRandom, resp);
    }

    private void write(ScopedRandom random, HttpServletResponse response) throws IOException {
        String result = String.format("%s (%s)%n", random.get(), random.getClass().getSimpleName());
        response.getWriter().write(result);
    }
}
