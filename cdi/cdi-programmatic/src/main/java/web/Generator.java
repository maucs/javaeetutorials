package web;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Random;

@SessionScoped
public class Generator implements Serializable {
    private int number;

    @PostConstruct
    public void init() {
        Random random = new Random();
        number = random.nextInt();
    }

    public int getNumber() {
        return number;
    }
}
