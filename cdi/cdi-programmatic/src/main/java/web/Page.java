package web;

import org.omnifaces.util.Faces;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.CDI;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Two ways of retrieving it
 */
@Named
@RequestScoped
public class Page {
    @Inject
    Instance<Generator> g;

    public int getNumber() {
        g.get().getNumber();
        // or
        Generator generator = CDI.current().select(Generator.class).get();
        return generator.getNumber();
    }

    public void reset(ActionEvent event) {
        Faces.invalidateSession();
    }
}
