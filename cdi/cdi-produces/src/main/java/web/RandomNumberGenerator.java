package web;

import javax.enterprise.inject.Produces;
import java.util.Random;

public class RandomNumberGenerator {
    private Random random = new Random();

    @Produces
    @Small
    int nextSmall() {
        return 4;
    }

    @Produces
    @Big
    int next() {
        return random.nextInt(10000000);
    }
}
