package web;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "BasicServlet", value = "/basic")
public class BasicServlet extends HttpServlet {
    // injecting static values
    @Inject
    @Small
    private int smallNumber;

    // injecting values that changes on every use
    @Inject
    @Big
    private Instance<Integer> bigNumber;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().write(String.valueOf(smallNumber) + "\n");
        resp.getWriter().write(String.valueOf(bigNumber.get()) + "\n");
    }
}
