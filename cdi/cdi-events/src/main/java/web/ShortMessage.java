package web;

public class ShortMessage {
    public String from, to, message;

    public ShortMessage(String from, String to, String message) {
        this.from = from;
        this.to = to;
        this.message = message;
    }
}
