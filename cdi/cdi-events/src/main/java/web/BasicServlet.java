package web;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "BasicServlet", value = "/basic")
public class BasicServlet extends HttpServlet {
    @Inject
    @Normal
    Event<ShortMessage> normalEvent;

    @Inject
    @Urgent
    Event<ShortMessage> urgentEvent;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ShortMessage message = new ShortMessage("james", "john", "kick the ball to me");
        normalEvent.fire(message);
        urgentEvent.fire(message);
    }
}
