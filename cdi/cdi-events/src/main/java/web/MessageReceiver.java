package web;

import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;

public class MessageReceiver {
    public void normalEventHandler(@Observes(during = TransactionPhase.AFTER_COMPLETION) @Normal ShortMessage message) {
        System.out.println(message.from + " has said that " + message.message + " to " + message.to);
    }

    public void urgentEventHandler(@Observes @Urgent ShortMessage message) {
        String msg = String.format("%s shouted %s to %s", message.from, message.message, message.to);
        System.out.println(msg.toUpperCase());
    }
}
