package web;

public interface NumberGenerator {
    int get();
}
