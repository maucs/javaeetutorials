package web;

public class SmallNumberGenerator implements NumberGenerator {
    @Override
    public int get() {
        return 10;
    }
}
