package web;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

@Decorator
public class BigNumberGenerator implements NumberGenerator {
    @Inject
    @Delegate
    NumberGenerator generator;

    @Override
    public int get() {
        return generator.get() * 10;
    }
}
