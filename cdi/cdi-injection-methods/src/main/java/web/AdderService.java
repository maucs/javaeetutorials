package web;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.New;
import javax.inject.Inject;
import javax.inject.Named;

@Named("adderService")
@RequestScoped
public class AdderService {
    @Inject
    @New
    RandomNumberGenerator generator;
    private int number;
    private int standaloneNumber;

    @Inject
    public AdderService(final RandomNumberGenerator g1, final RandomNumberGenerator g2) {
        this.number = g1.getNumber() + g2.getNumber();
    }

    public int getNumber() {
        return number;
    }

    public int getStandaloneNumber() {
        return standaloneNumber;
    }

    @Inject
    @New
    public void setStandaloneNumber(RandomNumberGenerator g) {
        this.standaloneNumber = g.getNumber();
    }

    public int getGeneratedStandaloneNumber() {
        return generator.getNumber();
    }
}
