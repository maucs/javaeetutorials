package web;

import javax.enterprise.context.RequestScoped;
import java.util.Random;

@RequestScoped
public class RandomNumberGenerator {
    private int number = new Random().nextInt(1000);

    public int getNumber() {
        return number;
    }
}
