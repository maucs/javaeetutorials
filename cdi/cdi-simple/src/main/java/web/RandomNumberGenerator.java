package web;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import java.util.Random;

@RequestScoped
public class RandomNumberGenerator {
    // this works too
    private int number;
    // = new Random().nextInt(1000);

    // allows fine-grained logic
    @PostConstruct
    public void init() {
        number = new Random().nextInt(1000);
    }

    @PreDestroy
    public void destroy() {
        System.out.println(getClass().getSimpleName() + ": Destroyed");
    }

    public int getNumber() {
        return number;
    }
}
