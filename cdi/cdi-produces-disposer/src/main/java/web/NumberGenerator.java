package web;

import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;

public class NumberGenerator {
    @Produces
    @Big
    public int get() {
        return 1000;
    }

    public void destroy(@Disposes @Big int number) {
        System.out.println(number + " is destroyed");
    }
}
