package web;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

@Model
public class Bean {
    @Inject
    @Big
    int number;

    public int getNumber() {
        return number;
    }
}
